// /** @type {import('next').NextConfig} */  
// // next.config.js
// module.exports = {
//   reactStrictMode: true,
//   experimental:{
//     serverMinification:false
//   },
//   swcMinify: true,
//   modularizeImports: {
//     '@mui/icons-material': {
//       transform: '@mui/icons-material/{{member}}',
//     },
//   },
//   async headers() {
//     return [
//       {
//         source: '/api/revalidate',
//         headers: [
//           {
//             key: 'Cache-Control',
//             value: 'no-store, max-age=0, must-revalidate'
//           },
//         ],
//       },
//     ]
//   },
//   images: {
//     domains: ['localhost'],
//     remotePatterns: [
//       {
//         protocol: 'http',
//         hostname: 'example.com',
//         port: '8000',
//         pathname: '/images/**',
//       },
//     ],
//   },
// }


/** @type {import('next').NextConfig} */  
module.exports = {
  reactStrictMode: true,
  experimental: {
    serverMinification: false
  },
  swcMinify: true,
  modularizeImports: {
    '@mui/icons-material': {
      transform: '@mui/icons-material/{{member}}',
    },
  },
  async headers() {
    return [
      {
        source: '/api/revalidate',
        headers: [
          {
            key: 'Cache-Control',
            value: 'no-store, max-age=0, must-revalidate'
          },
        ],
      },
    ]
  },

  images: {
    domains: ['localhost'],
    remotePatterns: [
      {
        protocol: 'http',
        hostname: 'localhost',
        port: '8000',
        pathname: '/slideshow/**',
      },
      {
        protocol: 'http',
        hostname: 'example.com',
        port: '8000',
        pathname: '/images/**',
      },
      {
        protocol: 'http',
        hostname: 'example.com',
        port: '8000',
        pathname: '/music/**',
      },
      {
        protocol: 'http',
        hostname: 'example.com',
        port: '8000',
        pathname: '/playlist/**',
      },
    ],
  },
}
