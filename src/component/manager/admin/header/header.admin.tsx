'use client'

import { useState } from 'react';
import Box from '@mui/material/Box';
import Drawer from '@mui/material/Drawer';
import CssBaseline from '@mui/material/CssBaseline';
import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import List from '@mui/material/List';
import Typography from '@mui/material/Typography';
import Divider from '@mui/material/Divider';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import Link from 'next/link';
import PersonIcon from '@mui/icons-material/Person';
import CommentIcon from '@mui/icons-material/Comment';
import ThumbUpAltIcon from '@mui/icons-material/ThumbUpAlt';
import PlaylistAddIcon from '@mui/icons-material/PlaylistAdd';
import AudiotrackIcon from '@mui/icons-material/Audiotrack';
import Image from 'next/image';
import IconButton from '@mui/material/IconButton';
import Menu from '@mui/material/Menu';
import Container from '@mui/material/Container';
import Avatar from '@mui/material/Avatar';
import MenuItem from '@mui/material/MenuItem';
import "../../../../../public/css/admin/header.admin.scss"
import "../../../../../public/css/admin/global.scss"
import { signOut, useSession } from "next-auth/react"

export default function AppHeaderAdmin({ children }: any) {
    const { data: session } = useSession();
    const drawerWidth = 240;
    const [anchorElUser, setAnchorElUser] = useState<null | HTMLElement>(null);

    const handleOpenUserMenu = (event: React.MouseEvent<HTMLElement>) => {
        setAnchorElUser(event.currentTarget);
    };
    const handleCloseUserMenu = () => {
        setAnchorElUser(null);
    };
    return (
        <Box sx={{ display: 'flex' }}>
            <CssBaseline />
            <AppBar
                position="fixed"
                sx={{ width: `calc(100% - ${drawerWidth}px)`, ml: `${drawerWidth}px` }}
            >
                <Container maxWidth="xl">
                    <Toolbar disableGutters>
                        {session ? <Box sx={{ flexGrow: 0 }}>
                            <IconButton onClick={handleOpenUserMenu} sx={{ p: 0 }}>
                                <Avatar alt="Remy Sharp" src="/static/images/avatar/2.jpg" />
                            </IconButton>

                            <Menu
                                sx={{ mt: '45px' }}
                                id="menu-appbar"
                                anchorEl={anchorElUser}
                                anchorOrigin={{
                                    vertical: 'top',
                                    horizontal: 'right',
                                }}
                                keepMounted
                                transformOrigin={{
                                    vertical: 'top',
                                    horizontal: 'right',
                                }}
                                open={Boolean(anchorElUser)}
                                onClose={handleCloseUserMenu}
                            >
                                <MenuItem onClick={handleCloseUserMenu}>
                                    <Typography textAlign="center"><Link className='link-option' href={"/"}>Trang chủ</Link></Typography>
                                </MenuItem>
                                <MenuItem onClick={() => {
                                    handleCloseUserMenu(),
                                        signOut()
                                }
                                }>
                                    Đăng xuất
                                </MenuItem>
                            </Menu>
                        </Box> : <Link href={"http://localhost:3000/auth/signin"}>Login</Link>}
                    </Toolbar>
                </Container>
            </AppBar>
            <Drawer
                sx={{
                    width: drawerWidth,
                    flexShrink: 0,
                    '& .MuiDrawer-paper': {
                        width: drawerWidth,
                        boxSizing: 'border-box',
                    },
                }}
                variant="permanent"
                anchor="left"
                className='nav-admin'
            >
                <Toolbar>
                    <Box>
                        <div style={{
                            display: 'flex',
                            alignItems: "center",
                        }}
                        >
                            <Image
                                src={"/image/icon-zing-admin.png"}
                                alt='Không có ảnh'
                                width={50}
                                height={50}
                            />
                            <h2>ADMIN</h2>
                        </div>
                    </Box>
                </Toolbar>
                <Divider />
                <List>
                    <Link href="/admin/comment" className='link-menu-admin'>
                        <ListItemButton >
                            <ListItemIcon>
                                <CommentIcon />
                            </ListItemIcon>
                            <ListItemText primary="Comments" />
                        </ListItemButton>
                    </Link>
                    <Link href="/admin/like" className='link-menu-admin'>
                        <ListItemButton >
                            <ListItemIcon>
                                <ThumbUpAltIcon />
                            </ListItemIcon>
                            <ListItemText primary="Like" />
                        </ListItemButton>
                    </Link>
                    <Link href="/admin/playlist" className='link-menu-admin'>
                        <ListItemButton >
                            <ListItemIcon>
                                <PlaylistAddIcon />
                            </ListItemIcon>
                            <ListItemText primary="Playlist" />
                        </ListItemButton>
                    </Link>
                    <Link href="/admin/track" className='link-menu-admin'>
                        <ListItemButton >
                            <ListItemIcon>
                                <AudiotrackIcon />
                            </ListItemIcon>
                            <ListItemText primary="Track" />
                        </ListItemButton>
                    </Link>
                    <Link href="/admin/user" className='link-menu-admin'>
                        <ListItemButton >
                            <ListItemIcon>
                                <PersonIcon />
                            </ListItemIcon>
                            <ListItemText primary="User" />
                        </ListItemButton>
                    </Link>
                </List>
            </Drawer>

            <Box
                component="main"
                sx={{ flexGrow: 1, bgcolor: 'background.default', p: 3 }}
            >
                <main>
                    <Toolbar />
                    <>
                        {children}
                    </>
                </main>
            </Box>
        </Box>
    );
}
