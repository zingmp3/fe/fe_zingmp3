'use client'

import { useEffect, useState } from 'react';

import Paper from '@mui/material/Paper';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TablePagination from '@mui/material/TablePagination';
import TableRow from '@mui/material/TableRow';
import { Alert, Autocomplete, Snackbar, Stack, TableCell, TextField } from '@mui/material';
import { useSession } from 'next-auth/react';
import "../../../../../public/css/admin/global.scss"
import DeleteModal from './comment.delete';
import { handleChangeDataTableNextPageComment, handleDeleteComment, handleSearchComment } from '@/utils/api/admin/comment.api';

interface IProps {
  data: ITrackComment[],
  meta: {
    current: number,
    pageSize: number,
    pages: number,
    total: number
  }
}

const CommentSub = (props: IProps) => {
  const { data, meta } = props;
  const { data: session } = useSession()

  const [metaData, setMetaData] = useState(meta);
  const [comment, setComment] = useState<ITrackComment[]>(data);
  // console.log("check comment:", comment)
  const [page, setPage] = useState<number>(metaData?.current ? metaData.current - 1 : 0);
  const [rowsPerPage, setRowsPerPage] = useState(metaData.pageSize);
  const [commentSuggestions, setCommentSuggestions] = useState<ITrackComment[]>([]);
  const [searchQuery, setSearchQuery] = useState<string>('');
  const [initialComments, setInitialComments] = useState<ITrackComment[]>([]); // Lưu trữ dữ liệu ban đầu


  // handle Model Delete
  /*  sử dụng cùng một openDelete state cho tất cả các dòng trong comment?.map" là chỉ có một biến boolean openDelete để điều khiển trạng thái mở/đóng của modal.
   Khi chỉ có một biến boolean để quản lý trạng thái của nhiều modal, tất cả các modal sẽ dùng chung trạng thái này. Điều này dẫn đến việc khi mở modal cho một hàng, tất cả các modal đều sẽ chia sẻ cùng một trạng thái mở/đóng,  nên modal sẽ không hoạt động đúng cho từng hàng riêng lẻ. */

  //  Thay đổi openDelete thành một đối tượng để lưu trữ trạng thái mở/đóng của từng modal theo columnId.
  const [openDelete, setOpenDelete] = useState({});

  const handleOpenDelete = (columnId: string) => {
    setOpenDelete({ ...openDelete, [columnId]: true });
  };

  const handleCloseDelete = (columnId: string) => {
    setOpenDelete({ ...openDelete, [columnId]: false });
  };

  const [openSnack, setOpenSnack] = useState(false);
  const handleCloseSnack = (event?: React.SyntheticEvent | Event, reason?: string) => {
    if (reason === 'clickaway') {
      return;
    }
    setOpenSnack(false);
  };

  // handle Pagination
  const handleChangePage = (event: unknown, newPage: number) => {  // tham số newPage là của TablePagination nó tự ném về bằng giá trị page (0 <= < ; > => 1,2,3...) khi tiến lên lùi xuống
    console.log('handleChangePage called with newPage:', newPage);
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event: React.ChangeEvent<HTMLInputElement>) => {
    const newRowsPerPage = parseInt(event.target.value, 10);
    console.log('handleChangeRowsPerPage called with newRowsPerPage:', newRowsPerPage);
    setRowsPerPage(newRowsPerPage);
  };

  //  Phần Search
  const handleSearchChange = async (event: React.ChangeEvent<HTMLInputElement>) => {
    await handleSearchComment(
      event.target.value,
      setSearchQuery,
      page,
      rowsPerPage,
      session?.access_token,
      setComment,
      setMetaData,
      setCommentSuggestions,
      meta,
      initialComments
    );
  }

  useEffect(() => {
    setInitialComments(data); // Lưu trữ dữ liệu ban đầu 
  }, [data]);

  //  Set lại trang khi chuyển trang
  useEffect(() => {
    const handleChangeDataTable = async () => {
      await handleChangeDataTableNextPageComment(page, rowsPerPage, session?.access_token, setComment, setMetaData);
    };

    handleChangeDataTable();
  }, [page, rowsPerPage]);

  const handleDeleteId = async (id: string) => {
    await handleDeleteComment(
      id,
      page,
      session?.access_token,
      setComment,
      setMetaData,
      setOpenSnack,
      setOpenDelete,
    );
  }
  console.log("check comment", comment)

  return (
    <>
      <Paper sx={{ width: '100%', overflow: 'hidden' }}>
        <br />
        <Autocomplete
          freeSolo //  Cho phép nhập bất kì giá trị không có trong danh sách gợi ý. Nếu true, người dùng có thể nhập bất kỳ giá trị nào.
          options={commentSuggestions}
          getOptionLabel={(option) => typeof option === 'string' ? option : option.content}
          onChange={(event, newValue) => newValue}
          renderInput={(params) => (
            <TextField
              {...params}
              label="Search Content"
              placeholder='Search...'
              variant="outlined"
              onChange={handleSearchChange}
              value={searchQuery}
            />
          )}
        />
        <TableContainer sx={{ maxHeight: '100%', textAlign: "center" }}>
          <Table stickyHeader aria-label="sticky table">
            <TableHead>
              <TableRow>
                <TableCell align="center">Content</TableCell>
                <TableCell align="center">User</TableCell>
                <TableCell align="center">Action</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>

              {comment?.map((column) => (
                <TableRow key={column._id}>
                  <TableCell align="center">{column.content}</TableCell>
                  <TableCell align="center">{column.userId?.email || ""}</TableCell>
                  <TableCell align="center">
                    <Stack direction={"row"} spacing={2} justifyContent="center">
                      <DeleteModal
                        openDelete={openDelete} // Đảm bảo mỗi modal được mở độc lập}
                        handleOpenDelete={() => handleOpenDelete(column._id)}
                        handleCloseDelete={() => handleCloseDelete(column._id)}
                        content={column.content}
                        columnId={column._id}
                        handleDeleteId={handleDeleteId}
                      />
                    </Stack>
                  </TableCell>

                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[10, 15, 20]}  //  Đây là một mảng các tùy chọn cho số lượng bản ghi hiển thị trên mỗi trang.
          component="div"   //  div" có nghĩa là TablePagination sẽ được hiển thị bên trong một thẻ <div>
          count={metaData.total}  //  Tổng số bản ghi 
          rowsPerPage={rowsPerPage}  // Số lượng bản ghi hiện đang được hiển thị trên mỗi trang.
          page={page}  //  Số trang hiện tại.  tức là nếu người dùng đang ở trang đầu tiên, page sẽ là 0
          onPageChange={handleChangePage}  // Hàm callback được gọi khi người dùng thay đổi trang.
          onRowsPerPageChange={handleChangeRowsPerPage}  //  Hàm callback được gọi khi người dùng thay đổi số lượng bản ghi hiển thị trên mỗi trang.
        />
      </Paper>

      <Snackbar
        open={openSnack}
        autoHideDuration={3000}
        onClose={handleCloseSnack}
        anchorOrigin={{ vertical: "top", horizontal: "center" }}
      >
        <Alert
          onClose={handleCloseSnack}
          severity="error"
          sx={{ width: '100%' }}
        >
          Comment delete successfully !
        </Alert>
      </Snackbar>

    </>
  );
}

export default CommentSub