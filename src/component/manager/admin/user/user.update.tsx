'use client'

import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import Modal from '@mui/material/Modal';
import { MenuItem, TextField } from '@mui/material';

export default function EditModal(props: any) {
  const {
    // props open/close
    openEdit,
    handleCloseEdit,
    handleOpenEdit,
    columnId,

    // props error
    erTextEmail,
    erEmail,
    erTextName,
    erName,
    erTextAge,
    erAge,
    erTextAddress,
    erAddress,
    erTextType,
    erType,
    erTextRole,
    erRole,

    // props edit submit
    handleUpdate,

    // props data field
    currentData,
    setCurrentData,
    arrRole,
    arrType,
  } = props;

  return (
    <>
      <Button className='edit-row' onClick={() => handleOpenEdit(columnId)}>Edit</Button>
      <Modal
        open={openEdit[columnId]}
        onClose={() => handleCloseEdit(columnId)}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >

        <Box className="content-modal">
          <Typography id="modal-modal-title" variant="h4" component="h2">
            Edit User
          </Typography>
          <Box sx={{ mt: 3, lineHeight: 4 }}>
            <TextField
              onKeyUp={(e) => {
                if (e.key === "Enter") {
                  handleUpdate(columnId)
                }
              }}
              fullWidth
              value={currentData.email}
              onChange={(e) => setCurrentData({ ...currentData, email: e.target.value })}
              label="Email"
              variant="standard"
              helperText={erTextEmail}
              error={erEmail}
            />

            <TextField
              onKeyUp={(e) => {
                if (e.key === "Enter") {
                  handleUpdate(columnId)
                }
              }}
              fullWidth
              value={currentData.name}
              onChange={(e) => setCurrentData({ ...currentData, name: e.target.value })}
              label="Name"
              variant="standard"
              helperText={erTextName}
              error={erName}
            />

            <TextField
              onKeyUp={(e) => {
                if (e.key === "Enter") {
                  handleUpdate(columnId)
                }
              }}
              fullWidth
              value={currentData.age}
              onChange={(e) => setCurrentData({ ...currentData, age: e.target.value })}
              label="Age"
              variant="standard"
              helperText={erTextAge}
              error={erAge}
            />

            <TextField
              onKeyUp={(e) => {
                if (e.key === "Enter") {
                  handleUpdate(columnId)
                }
              }}
              fullWidth
              value={currentData.address}
              onChange={(e) => setCurrentData({ ...currentData, address: e.target.value })}
              label="Address"
              variant="standard"
              helperText={erTextAddress}
              error={erAddress}
            />

            <TextField
              onKeyUp={(e) => {
                if (e.key === "Enter") {
                  handleUpdate(columnId)
                }
              }}
              fullWidth
              select
              value={currentData.type}
              onChange={(e) => setCurrentData({ ...currentData, type: e.target.value })}
              label="Type"
              variant="standard"
              helperText={erTextType}
              error={erType}
            >
              {arrType.map((option: any) => (
                <MenuItem key={option.value} value={option.value}>
                  {option.label}
                </MenuItem>
              ))}
            </TextField>

            <TextField
              onKeyUp={(e) => {
                if (e.key === "Enter") {
                  handleUpdate()
                }
              }}
              fullWidth
              select
              value={currentData.role}
              onChange={(e) => setCurrentData({ ...currentData, role: e.target.value })}
              label="Role"
              variant="standard"
              helperText={erTextRole}
              error={erRole}
            >
              {arrRole.map((option: any) => (
                <MenuItem key={option.value} value={option.value}>
                  {option.label}
                </MenuItem>
              ))}
            </TextField>

            <Box className="add-btn">
              <Button variant='contained' className='add-submit' onClick={() => handleUpdate(columnId)}>Submit</Button>
            </Box>

          </Box>
        </Box>
      </Modal>

    </>
  );
}
