'use client'

import { useEffect, useState } from 'react';
import Paper from '@mui/material/Paper';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TablePagination from '@mui/material/TablePagination';
import TableRow from '@mui/material/TableRow';
import { Alert, Autocomplete, Snackbar, Stack, TableCell, TextField } from '@mui/material';
import { useSession } from 'next-auth/react';
import "../../../../../public/css/admin/user.table.scss"
import AddModal from './user.create';
import DeleteModal from './user.delete';
import EditModal from './user.update';
import { handleChangeDataTableNextPageUser, handleSearchUser, queryCreateUser, queryDeleteUser, queryEditUser } from '@/utils/api/admin/use.api';


interface IProps {
  data: IUser[],
  meta: {
    current: number,
    pageSize: number,
    pages: number,
    total: number
  }
}


const UserSub = (props: IProps) => {

  const { data, meta } = props;
  const { data: session } = useSession()
  // console.log("check meta:", meta)

  const [metaData, setMetaData] = useState(meta);
  const [user, setUser] = useState<IUser[]>(data);
  const [page, setPage] = useState<number>(metaData?.current ? metaData.current - 1 : 0);
  const [rowsPerPage, setRowsPerPage] = useState(metaData.pageSize);


  const [userSuggestions, setUserSuggestions] = useState<IUser[]>([]);
  const [searchQuery, setSearchQuery] = useState<string>('');
  const [initialUser, setInitialUser] = useState<IUser[]>([]); // Lưu trữ dữ liệu ban đầu


  // Validate ERROR
  const [erTextEmail, setErTextEmail] = useState<string>("");
  const [erEmail, setErEmail] = useState<boolean>(false);

  const [erTextPassword, setErTextPassword] = useState<string>("");
  const [erPassword, setErPassword] = useState<boolean>(false);

  const [erTextName, setErTextName] = useState<string>("");
  const [erName, setErName] = useState<boolean>(false);

  const [erTextAge, setErTextAge] = useState<string>("");
  const [erAge, setErAge] = useState<boolean>(false);

  const [erTextAddress, setErTextAddress] = useState<string>("");
  const [erAddress, setErAddress] = useState<boolean>(false);

  const [erTextType, setErTextType] = useState<string>("");
  const [erType, setErType] = useState<boolean>(false);

  const [erTextRole, setErTextRole] = useState<string>("");
  const [erRole, setErRole] = useState<boolean>(false);

  // value field
  const [email, setEmail] = useState<string>("");
  const [password, setPassword] = useState<string>("");
  const [name, setName] = useState<string>("");
  const [age, setAge] = useState<string>("");
  const [address, setAddress] = useState<string>("");
  const [type, setType] = useState<string>("");
  const [role, setRole] = useState<string>("");

  // handle Add 
  const [open, setOpen] = useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  // Model Delete
  const [openDelete, setOpenDelete] = useState({});

  const handleOpenDelete = (columnId: string) => {
    setOpenDelete({ ...openDelete, [columnId]: true });
  };

  const handleCloseDelete = (columnId: string) => {
    setOpenDelete({ ...openDelete, [columnId]: false });
  };


  // Model Edit
  const [currentData, setCurrentData] = useState({
    email: '',
    name: '',
    age: 0,
    address: '',
    type: '',
    role: ''
  });
  const [openEdit, setOpenEdit] = useState({});

  const handleOpenEdit = (column: IUser) => {
    setOpenEdit((prev) => ({ ...prev, [column._id]: true }));
    setCurrentData({
      email: column.email,
      name: column.name,
      age: column.age,
      address: column.address,
      type: column.type,
      role: column.role
    });
  };

  const handleCloseEdit = (columnId: string) => {
    setOpenEdit({ ...openEdit, [columnId]: false });
  };

  //  states open/close Snack
  const [openSnackDelete, setOpenSnackDelete] = useState(false);
  const handleCloseSnackDelete = (event?: React.SyntheticEvent | Event, reason?: string) => {
    if (reason === 'clickaway') {
      return;
    }
    setOpenSnackDelete(false);
  };


  const [openSnackAdd, setOpenSnackAdd] = useState(false);
  const handleCloseSnackAdd = (event?: React.SyntheticEvent | Event, reason?: string) => {
    if (reason === 'clickaway') {
      return;
    }
    setOpenSnackAdd(false);
  };


  const [openSnackEdit, setOpenSnackEdit] = useState(false);
  const handleCloseSnackEdit = (event?: React.SyntheticEvent | Event, reason?: string) => {
    if (reason === 'clickaway') {
      return;
    }
    setOpenSnackEdit(false);
  };

  const arrRole = [
    {
      value: 'USER',
      label: 'USER',
    },
    {
      value: 'ADMIN',
      label: 'ADMIN',
    },

  ];

  const arrType = [
    {
      value: 'SYSTEM',
      label: 'SYSTEM',
    },
    {
      value: 'GITHUB',
      label: 'GITHUB',
    },
    {
      value: 'GOOGLE',
      label: 'GOOGLE',
    },
  ];

  // Handle pagination
  const handleChangePage = (event: unknown, newPage: number) => {  // tham số newPage là của TablePagination nó tự ném về bằng giá trị page (0 <= < ; > => 1,2,3...) khi tiến lên lùi xuống
    console.log('handleChangePage called with newPage:', newPage);
    setPage(newPage);
  };


  const handleChangeRowsPerPage = (event: React.ChangeEvent<HTMLInputElement>) => {
    const newRowsPerPage = parseInt(event.target.value, 10);
    console.log('handleChangeRowsPerPage called with newRowsPerPage:', newRowsPerPage);
    setRowsPerPage(newRowsPerPage);
  };


  //  Phần Search
  const handleSearchChange = async (event: React.ChangeEvent<HTMLInputElement>) => {
    await handleSearchUser(
      event.target.value,
      setSearchQuery,
      page,
      rowsPerPage,
      session?.access_token,
      setUser,
      setMetaData,
      setUserSuggestions,
      meta,
      initialUser,
    )
  }

  useEffect(() => {
    setInitialUser(data); // Lưu trữ dữ liệu ban đầu 
  }, [data]);



  //  Set lại trang khi chuyển trang
  useEffect(() => {
    const handleChangeDataTable = async () => {

      await handleChangeDataTableNextPageUser(
        page,
        rowsPerPage,
        session?.access_token,
        setUser,
        setMetaData,
      )
    }
    handleChangeDataTable();
  }, [page, rowsPerPage]);


  // Query create
  const handleCreate = async () => {
    setErTextEmail("");
    setErTextPassword("")
    setErTextName("");
    setErTextAge("");
    setErTextAddress("");
    setErTextType("");
    setErTextRole("");
    setErEmail(false);
    setErPassword(false)
    setErName(false);
    setErAge(false);
    setErType(false);
    setErAddress(false);
    setErRole(false);

    // Check validate exist
    const emailExists = user.some(user => user.email === email);
    if (emailExists) {
      setErTextEmail("Email already exists");
      setErEmail(true);
      return false;
    }

    else if (!email) {
      setErTextEmail("Email is not empty")
      setErEmail(true);
      return false
    }

    else if (!password) {
      setErTextPassword("Password is not empty")
      setErPassword(true);
      return false
    }

    else if (!name) {
      setErTextName("Name is not empty")
      setErName(true);
      return false
    }

    else if (!age) {
      setErTextAge("Age is not empty")
      setErAge(true);
      return false
    }

    else if (!address) {
      setErTextAddress("Address is not empty")
      setErAddress(true);
      return false
    }

    else if (!type) {
      setErTextType("Type is not empty")
      setErType(true);
      return false
    }

    else if (!role) {
      setErTextRole("Role is not empty")
      setErRole(true);
      return false
    }

    else if (email && password && name && age && address && type && role) {
      await queryCreateUser(
        session?.access_token,
        email,
        password,
        name,
        age,
        address,
        type,
        role,
        setUser,
        setMetaData,
        setOpen,
        setOpenSnackAdd,
        setEmail,
        setPassword,
        setName,
        setAge,
        setAddress,
        setType,
        setRole,
      )
    }
  }

  //  Query edit
  const handleUpdate = async (id: string) => {
    setErTextEmail("");
    setErTextName("");
    setErTextAge("");
    setErTextAddress("");
    setErTextType("");
    setErTextRole("");
    setErEmail(false);
    setErName(false);
    setErAge(false);
    setErType(false);
    setErAddress(false);
    setErRole(false);

    // Check validate exist
    const emailExists = user.some(user => user.email === currentData.email);
    if (emailExists) {
      setErTextEmail("Email already exists");
      setErEmail(true);
      return false;
    }

    else if (!currentData.email) {
      setErTextEmail("Email is not empty")
      setErEmail(true);
      return false
    }

    else if (!currentData.name) {
      setErTextName("Name is not empty")
      setErName(true);
      return false
    }

    else if (!currentData.age) {
      setErTextAge("Age is not empty")
      setErAge(true);
      return false
    }

    else if (!currentData.address) {
      setErTextAddress("Address is not empty")
      setErAddress(true);
      return false
    }

    else if (!currentData.type) {
      setErTextType("Type is not empty")
      setErType(true);
      return false
    }

    else if (!currentData.role) {
      setErTextRole("Role is not empty")
      setErRole(true);
      return false
    }

    else if (currentData.email && currentData.name && currentData.age && currentData.address && currentData.type && currentData.role) {
      console.log("check currentData.role", currentData.role,)
      await queryEditUser(
        id,
        session?.access_token,
        page,
        currentData,
        setUser,
        setMetaData,
        setOpenEdit,
        setOpenSnackEdit,
      )
    }
  }

  //  Query Delete
  const handleDelete = async (id: string) => {
    await queryDeleteUser(
      id,
      session?.access_token,
      page,
      setUser,
      setMetaData,
      setOpenSnackDelete,
      setOpenDelete,
    )
  }

  return (
    <>
      <Paper sx={{ width: '100%', overflow: 'hidden' }}>
        <Stack direction={"row"} m={2} justifyContent="flex-end">

          <AddModal
            // close and open modal
            open={open}
            handleClose={handleClose}
            handleOpen={handleOpen}

            // create user
            handleCreate={handleCreate}

            // error message
            erTextEmail={erTextEmail}
            erEmail={erEmail}
            erTextPassword={erTextPassword}
            erPassword={erPassword}
            erTextName={erTextName}
            erName={erName}
            erTextAge={erTextAge}
            erAge={erAge}
            erTextAddress={erTextAddress}
            erAddress={erAddress}
            erTextType={erTextType}
            erType={erType}
            erTextRole={erTextRole}
            erRole={erRole}

            // value field
            email={email}
            setEmail={setEmail}
            password={password}
            setPassword={setPassword}
            name={name}
            setName={setName}
            age={age}
            setAge={setAge}
            address={address}
            setAddress={setAddress}
            type={type}
            setType={setType}
            role={role}
            setRole={setRole}
            arrRole={arrRole}
            arrType={arrType}
          />
        </Stack>

        <Autocomplete
          freeSolo //  Cho phép nhập bất kì giá trị không có trong danh sách gợi ý. Nếu true, người dùng có thể nhập bất kỳ giá trị nào.
          options={userSuggestions}  // 
          getOptionLabel={(option) => typeof option === 'string' ? option : option.email}  //  hiển thị gợi ý
          onChange={(event, newValue) => newValue}

          renderInput={(params) => (

            <TextField
              {...params}
              label="Search Email"
              placeholder='Search...'
              variant="outlined"
              onChange={handleSearchChange}
              value={searchQuery}
            />
          )}
        />
        <TableContainer sx={{ maxHeight: '100%', textAlign: "center" }}>
          <Table stickyHeader aria-label="sticky table">
            <TableHead>
              <TableRow>
                <TableCell align="center">Email</TableCell>
                <TableCell align="center">Name</TableCell>
                <TableCell align="center">Age</TableCell>
                <TableCell align="center">Address</TableCell>
                <TableCell align="center">Type</TableCell>
                <TableCell align="center">Role</TableCell>
                <TableCell align="center">Active</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>

              {user?.map((column) => (
                <TableRow key={column._id}>
                  <TableCell align="center">{column.email}</TableCell>
                  <TableCell align="center">{column.name}</TableCell>
                  <TableCell align="center">{column.age}</TableCell>
                  <TableCell align="center">{column.address}</TableCell>
                  <TableCell align="center">{column.type}</TableCell>
                  <TableCell align="center">{column.role}</TableCell>

                  <TableCell align="center">
                    <Stack direction={"row"} spacing={2} justifyContent="center">

                      <EditModal
                        // close and open modal
                        openEdit={openEdit}
                        columnId={column._id}
                        handleCloseEdit={() => handleCloseEdit(column._id)}
                        handleOpenEdit={() => handleOpenEdit(column)}

                        // Edit user
                        handleUpdate={handleUpdate}

                        // error message
                        erTextEmail={erTextEmail}
                        erEmail={erEmail}
                        erTextPassword={erTextPassword}
                        erPassword={erPassword}
                        erTextName={erTextName}
                        erName={erName}
                        erTextAge={erTextAge}
                        erAge={erAge}
                        erTextAddress={erTextAddress}
                        erAddress={erAddress}
                        erTextType={erTextType}
                        erType={erType}
                        erTextRole={erTextRole}
                        erRole={erRole}

                        // value field
                        email={email}
                        setEmail={setEmail}
                        name={name}
                        setName={setName}
                        age={age}
                        setAge={setAge}
                        address={address}
                        setAddress={setAddress}
                        type={type}
                        setType={setType}
                        role={role}
                        setRole={setRole}
                        arrRole={arrRole}
                        arrType={arrType}
                        // state in field update
                        currentData={currentData}
                        setCurrentData={setCurrentData}
                      />

                      <DeleteModal
                        openDelete={openDelete} // Đảm bảo mỗi modal được mở độc lập}
                        handleCloseDelete={() => handleCloseDelete(column._id)}
                        handleOpenDelete={() => handleOpenDelete(column._id)}
                        columnId={column._id}
                        columnEmail={column.email}
                        handleDelete={handleDelete}
                      />
                    </Stack>
                  </TableCell>
                </TableRow>
              ))}

            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[10, 15, 20]}  //  Đây là một mảng các tùy chọn cho số lượng bản ghi hiển thị trên mỗi trang.
          component="div"   //  div" có nghĩa là TablePagination sẽ được hiển thị bên trong một thẻ <div>
          count={metaData.total}  //  Tổng số bản ghi 
          rowsPerPage={rowsPerPage}  // Số lượng bản ghi hiện đang được hiển thị trên mỗi trang.
          page={page}  //  Số trang hiện tại.  tức là nếu người dùng đang ở trang đầu tiên, page sẽ là 0
          onPageChange={handleChangePage}  // Hàm callback được gọi khi người dùng thay đổi trang.
          onRowsPerPageChange={handleChangeRowsPerPage}  //  Hàm callback được gọi khi người dùng thay đổi số lượng bản ghi hiển thị trên mỗi trang.
        />
      </Paper>

      <Snackbar
        open={openSnackDelete}
        autoHideDuration={3000}
        onClose={handleCloseSnackDelete}
        anchorOrigin={{ vertical: "top", horizontal: "center" }}
      >
        <Alert
          onClose={handleCloseSnackDelete}
          severity="error"
          sx={{ width: '100%' }}
        >
          User delete successfully
        </Alert>
      </Snackbar>

      <Snackbar
        open={openSnackAdd}
        autoHideDuration={3000}
        onClose={handleCloseSnackAdd}
        anchorOrigin={{ vertical: "top", horizontal: "center" }}
      >
        <Alert
          onClose={handleCloseSnackAdd}
          severity="success"
          sx={{ width: '100%' }}
        >
          User add successfully
        </Alert>
      </Snackbar>

      <Snackbar
        open={openSnackEdit}
        autoHideDuration={3000}
        onClose={handleCloseSnackEdit}
        anchorOrigin={{ vertical: "top", horizontal: "center" }}
      >
        <Alert
          onClose={handleCloseSnackEdit}
          severity="warning"
          sx={{ width: '100%' }}
        >
          User edit successfully
        </Alert>
      </Snackbar>

    </>
  );
}

export default UserSub