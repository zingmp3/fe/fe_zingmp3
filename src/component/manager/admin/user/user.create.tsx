'use client'

import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import Modal from '@mui/material/Modal';
import AddIcon from '@mui/icons-material/Add';
import { MenuItem, TextField } from '@mui/material';

export default function AddModal(props: any) {
    const {
        // props open/close
        open,
        handleClose,
        handleOpen,

        // props error
        erTextEmail,
        erEmail,
        erTextPassword,
        erPassword,
        erTextName,
        erName,
        erTextAge,
        erAge,
        erTextAddress,
        erAddress,
        erTextType,
        erType,
        erTextRole,
        erRole,

        // props create submit
        handleCreate,

        // props data field
        email,
        setEmail,
        password,
        setPassword,
        name,
        setName,
        age,
        setAge,
        address,
        setAddress,
        type,
        setType,
        role,
        setRole,
        arrRole,
        arrType,
    } = props;
    return (
        <>
            <Button className='add-row' onClick={handleOpen}><AddIcon sx={{ fontSize: "20px" }} /> Add</Button>
            <Modal
                open={open}
                onClose={handleClose}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box className="content-modal">
                    <Typography id="modal-modal-title" variant="h4" component="h2">
                        Add User
                    </Typography>
                    <Box sx={{ mt: 3, lineHeight: 4 }}>
                        <TextField
                            onKeyUp={(e) => {
                                if (e.key === "Enter") {
                                    handleCreate()
                                }
                            }}
                            fullWidth
                            value={email}
                            onChange={(e) => setEmail(e.target.value)}
                            label="Email"
                            variant="standard"
                            helperText={erTextEmail}
                            error={erEmail}
                        />

                        <TextField
                            onKeyUp={(e) => {
                                if (e.key === "Enter") {
                                    handleCreate()
                                }
                            }}
                            fullWidth
                            value={password}
                            onChange={(e) => setPassword(e.target.value)}
                            label="Password"
                            variant="standard"
                            helperText={erTextPassword}
                            error={erPassword}
                        />

                        <TextField
                            onKeyUp={(e) => {
                                if (e.key === "Enter") {
                                    handleCreate();
                                }
                            }}
                            fullWidth
                            value={name}
                            onChange={(e) => setName(e.target.value)}
                            label="Name"
                            variant="standard"
                            helperText={erTextName}
                            error={erName}
                        />

                        <TextField
                            onKeyUp={(e) => {
                                if (e.key === "Enter") {
                                    handleCreate()
                                }
                            }}
                            fullWidth
                            value={age}
                            onChange={(e) => setAge(e.target.value)}
                            label="Age"
                            variant="standard"
                            helperText={erTextAge}
                            error={erAge}
                        />

                        <TextField
                            onKeyUp={(e) => {
                                if (e.key === "Enter") {
                                    handleCreate()
                                }
                            }}
                            fullWidth
                            value={address}
                            onChange={(e) => setAddress(e.target.value)}
                            label="Address"
                            variant="standard"
                            helperText={erTextAddress}
                            error={erAddress}
                        />

                        <TextField
                            onKeyUp={(e) => {
                                if (e.key === "Enter") {
                                    handleCreate()
                                }
                            }}
                            fullWidth
                            select
                            value={type}
                            onChange={(e) => setType(e.target.value)}
                            label="Type"
                            variant="standard"
                            helperText={erTextType}
                            error={erType}
                        >

                            {arrType.map((option: any) => (
                                <MenuItem key={option.value} value={option.value}>
                                    {option.label}
                                </MenuItem>
                            ))}
                        </TextField>



                        <TextField
                            onKeyUp={(e) => {
                                if (e.key === "Enter") {
                                    handleCreate()
                                }
                            }}
                            fullWidth
                            select
                            value={role}
                            onChange={(e) => setRole(e.target.value)}
                            label="Role"
                            variant="standard"
                            helperText={erTextRole}
                            error={erRole}
                        >
                            {arrRole.map((option: any) => (
                                <MenuItem key={option.value} value={option.value}>
                                    {option.label}
                                </MenuItem>
                            ))}
                        </TextField>

                        <Box className="add-btn">
                            <Button variant='contained' className='add-submit' onClick={() => handleCreate()}>Submit</Button>
                        </Box>

                    </Box>
                </Box>
            </Modal>
        </>
    );
}
