'use client'

import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Modal from '@mui/material/Modal';
import { Stack, Typography, } from '@mui/material';



export default function DeleteModal(props: any) {
    const {
        openDelete,
        handleCloseDelete,
        handleOpenDelete,
        columnId,
        columnEmail,
        handleDelete
    } = props;

    return (
        <>
            <Button onClick={() => handleOpenDelete(columnId)} className='delete-row'>Delete</Button>

            <Modal
                open={openDelete[columnId]}
                onClose={() => handleCloseDelete(columnId)}
                className="model-delete"
            >
                <Box className="content-modal content-delete">
                    <Typography variant="h6" component="h2">
                        Are want to you delete {columnEmail}?
                    </Typography>
                    <br />
                    <Box>
                        <Stack spacing={2} direction="row" justifyContent={"center"}>
                            <Button className='delete-row' onClick={() => handleDelete(columnId)}>Delete</Button>
                            <Button onClick={() => handleCloseDelete(columnId)} variant="outlined">Cancel</Button>
                        </Stack>
                    </Box>
                </Box>
            </Modal>
        </>
    );
}
