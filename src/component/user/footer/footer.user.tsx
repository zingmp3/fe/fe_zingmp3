'use client'

import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import AudioPlayer, { RHAP_UI } from 'react-h5-audio-player';
import 'react-h5-audio-player/lib/styles.css';
import Image from 'next/image';
import "../../../../public/css/user/footer.user.scss";
import MicExternalOnIcon from '@mui/icons-material/MicExternalOn';
import { useEffect, useRef } from 'react';
import { useTrackContext } from '@/lib/track.wrapper';
import Link from 'next/link';

const FooterUser = () => {
    const playRef = useRef(null)
    const { currentTrack, setCurrentTrack } = useTrackContext() as ITrackContext;

    useEffect(() => {
        if (currentTrack.isPlaying === true) {
            // @ts-ignore
            playRef?.current?.audio.current.play();
        }
        else if (currentTrack.isPlaying === false) {
            // @ts-ignore
            playRef?.current?.audio.current.pause();
        }
    }, [currentTrack])

    return (
        <>
            {currentTrack._id !== "string" && (
                <AppBar
                    position="fixed"
                    color="primary"
                    sx={{
                        top: 'auto',
                        bottom: 0,
                    }}>
                    <Box sx={{
                        display: 'flex',
                        // gap: "20px",
                        backgroundColor: "#130C1C",
                    }}>
                        <div className="track-content">
                            <Image
                                src={
                                    currentTrack.imgUrl === "/image/upload-image.png" ?
                                        currentTrack.imgUrl
                                        :
                                        `${process.env.NEXT_PUBLIC_BACKEND_URL}/img-song/${currentTrack.imgUrl}`
                                }
                                alt=""
                                width={100}
                                height={100}
                            />
                            <div className="track-info">

                                <div className="track-title">
                                    <Link href={`/track/${currentTrack._id}?audio=${currentTrack.trackUrl}`}>
                                        <h3 className="track-name" style={{ color: "#fff" }}>{currentTrack.title}</h3>
                                    </Link>
                                    <p style={{ color: "#fff" }}>{currentTrack.singer}</p>
                                </div>

                            </div>
                        </div>
                        <AudioPlayer
                            ref={playRef}
                            style={{
                                width: "50%",
                                boxShadow: "none",
                                margin: "auto",
                                backgroundColor: "#130C1C",
                            }}
                            autoPlay
                            src={
                                currentTrack.imgUrl === "/image/upload-image.png" ?
                                    `${process.env.NEXT_PUBLIC_BACKEND_URL}/upload/${currentTrack.trackUrl}`
                                    :
                                    `${process.env.NEXT_PUBLIC_BACKEND_URL}/music/${currentTrack.trackUrl} `}
                            // Cơ chế mặc định của AudioPlayer: Khi vào trang chi tiết bài hát  AudioPlayer bắt đầu phát nhạc, onPlay sẽ được gọi và isPlaying của currentTrack sẽ thành true
                            // Cơ chế hoạt động khi reload trang web: Khi reload trang web, trình duyệt sẽ không tự động chạy lại các sự kiện onPlay
                            onPlay={() => setCurrentTrack({ ...currentTrack, isPlaying: true })}
                            onPause={() => setCurrentTrack({ ...currentTrack, isPlaying: false })}
                            onEnded={() => setCurrentTrack({ ...currentTrack, isPlaying: false })}
                            layout='stacked-reverse'
                            customControlsSection={[
                                RHAP_UI.MAIN_CONTROLS,
                                RHAP_UI.LOOP,
                            ]}
                            customProgressBarSection={
                                [
                                    RHAP_UI.CURRENT_TIME,
                                    RHAP_UI.PROGRESS_BAR,
                                    RHAP_UI.CURRENT_LEFT_TIME,
                                    RHAP_UI.VOLUME,
                                    RHAP_UI.ADDITIONAL_CONTROLS,
                                ]
                            }
                            customAdditionalControls={[
                                <MicExternalOnIcon className='lyric' />
                            ]}
                        />
                    </Box>
                </AppBar>
            )}
        </>
    )
}

export default FooterUser