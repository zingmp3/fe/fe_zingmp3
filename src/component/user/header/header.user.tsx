'use client'

import { useEffect, useState } from 'react';
import Box from '@mui/material/Box';
import Drawer from '@mui/material/Drawer';
import CssBaseline from '@mui/material/CssBaseline';
import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import List from '@mui/material/List';
import Typography from '@mui/material/Typography';
import Divider from '@mui/material/Divider';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import Link from 'next/link';
import Image from 'next/image';
import IconButton from '@mui/material/IconButton';
import Menu from '@mui/material/Menu';
import Avatar from '@mui/material/Avatar';
import MenuItem from '@mui/material/MenuItem';
import "../../../../public/css/user/global.scss";
import "../../../../public/css/user/header.user.scss";
import SearchIcon from '@mui/icons-material/Search';
import { useSession, signOut } from "next-auth/react"
import LibraryMusicIcon from '@mui/icons-material/LibraryMusic';
import AlbumIcon from '@mui/icons-material/Album';
import { Autocomplete, InputBase, TextField, useMediaQuery } from '@mui/material';
import { styled, alpha } from '@mui/material/styles';
import { useRouter } from 'next/navigation';

interface IProps {
    track: ITracks[]
    children: React.ReactNode // Thêm kiểu của children
}

export default function AppHeaderUser({ track, children }: IProps) {
    const { data: session } = useSession();
    const router = useRouter();
    // console.log("check sesion:", session)

    const drawerWidth = 240;
    const smallDrawerWidth = 60; // Chiều rộng khi màn hình nhỏ
    // useMediaQuery để xác định kích thước màn hình và thay đổi giao diện dựa trên kích thước màn hình
    const isSmallScreen = useMediaQuery('(max-width:1024px)');
    const [anchorElUser, setAnchorElUser] = useState<null | HTMLElement>(null);

    const handleOpenUserMenu = (event: React.MouseEvent<HTMLElement>) => {
        setAnchorElUser(event.currentTarget);
    };

    const handleCloseUserMenu = () => {
        setAnchorElUser(null);
    };

    useEffect(() => {
        // Handle Preload ảnh để k bị nhấp nháp khi thay đổi
        const preloadImage = (src: string) => {
            const img = new window.Image();  //  khởi tạo 2 Image có src path là 2 ảnh 
            img.src = src;
        };
        preloadImage("/image/icon-zing-user.png");
        preloadImage("/image/logo_zing_2024-05-28 135406.png");
    }, []);

    const Search = styled('div')(({ theme }) => ({
        position: 'relative',
        backgroundColor: alpha(theme.palette.common.white, 0.15),
        '&:hover': {
            backgroundColor: alpha(theme.palette.common.white, 0.25),
        },
        marginRight: theme.spacing(2),
        marginLeft: 0,
        width: '100%',
        [theme.breakpoints.up('sm')]: {
            marginLeft: theme.spacing(3),
            width: 'auto',
        },
        borderRadius: "20px",
    }));

    const SearchIconWrapper = styled('div')(({ theme }) => ({
        padding: theme.spacing(0, 2),
        height: '100%',
        position: 'absolute',
        pointerEvents: 'none',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-start',
        color: "#fff",
        width: "100%",
        borderRadius: "20px",
        zIndex: 1
    }));

    return (
        <>
            <Box sx={{ display: 'flex' }}>
                <CssBaseline />
                <AppBar
                    position="fixed"
                    sx={{
                        width: `calc(100% - ${isSmallScreen ? smallDrawerWidth : drawerWidth}px)`,
                        ml: `${isSmallScreen ? smallDrawerWidth : drawerWidth}px`,  // ml => "margin-left".
                    }}
                >
                    <Toolbar
                        sx={{
                            display: "flex",
                            justifyContent: "space-between",
                            backgroundColor: "#170F23"
                        }}>
                        <Search>
                            <SearchIconWrapper>
                                <SearchIcon sx={{ color: "#fff" }} />
                            </SearchIconWrapper>
                            <Autocomplete
                                freeSolo //  Cho phép nhập bất kì giá trị không có trong danh sách gợi ý. Nếu true, người dùng có thể nhập bất kỳ giá trị nào.
                                options={track}
                                getOptionLabel={(option) => typeof option === 'string' ? option : option.title}
                                renderOption={(props, option) => {
                                    const { key, ...otherProps } = props as { key?: any } & React.HTMLProps<HTMLLIElement>; // Cast để tránh lỗi TypeScript
                                    return (
                                        <div key={option._id}>
                                            <li {...otherProps} style={{ position: 'relative', padding: 0 }}>
                                                <Link href={`/track/${option._id}`} style={{
                                                    textDecoration: 'none',
                                                    color: 'inherit',
                                                    display: 'block',
                                                    width: '100%',
                                                    height: '100%',
                                                    padding: '8px'
                                                }}>
                                                    {option.title}
                                                </Link>
                                            </li>
                                        </div>
                                    )
                                }
                                }
                                renderInput={(params) => (
                                    <TextField
                                        {...params}
                                        placeholder='Tìm kiếm bài hát…'
                                        onKeyDown={async (e: any) => {
                                            if (e.key === "Enter") {
                                                if (e?.target?.value)
                                                    console.log("check target:", e?.target?.value)
                                                window.location.href = `/search?q=${e?.target?.value}`
                                            }
                                        }}
                                        sx={{
                                            '& .MuiOutlinedInput-root': {
                                                '& fieldset': {
                                                    border: 'none', // Bỏ border
                                                },
                                                '&:hover fieldset': {
                                                    border: 'none', // Bỏ border khi hover
                                                },
                                                '&.Mui-focused fieldset': {
                                                    border: 'none', // Bỏ border khi focus
                                                },
                                                '& input': {
                                                    color: 'white', // Màu chữ trắng
                                                    caretColor: 'white', // Màu con trỏ trắng
                                                },
                                                '&::placeholder': {
                                                    color: '#fff',
                                                    opacity: 1,
                                                },
                                            },
                                        }}
                                    />
                                )}
                            />
                        </Search>

                        {session ? <Box sx={{ flexGrow: 0 }}>
                            <IconButton onClick={handleOpenUserMenu} sx={{ p: 0 }}>
                                <Avatar alt="Remy Sharp" src="/image/man.png" />
                            </IconButton>
                            <Menu
                                sx={{ mt: '45px' }}
                                id="menu-appbar"
                                anchorEl={anchorElUser}
                                anchorOrigin={{
                                    vertical: 'top',
                                    horizontal: 'right',
                                }}
                                keepMounted
                                transformOrigin={{
                                    vertical: 'top',
                                    horizontal: 'right',
                                }}
                                open={Boolean(anchorElUser)}
                                onClose={handleCloseUserMenu}
                                className='menu-bar-option'
                            >
                                <MenuItem onClick={handleCloseUserMenu}>
                                    <Typography textAlign="center"><Link className='link-option' href={"/admin"}>Trang chủ</Link></Typography>
                                </MenuItem>
                                <MenuItem onClick={() => {
                                    handleCloseUserMenu(),
                                        signOut()
                                }
                                }>
                                    Đăng xuất
                                </MenuItem>
                            </Menu>
                        </Box> : <Link href={"http://localhost:3000/auth/signin"}>Login</Link>}
                    </Toolbar>
                </AppBar>
                <Drawer
                    sx={{
                        width: isSmallScreen ? smallDrawerWidth : drawerWidth,
                        flexShrink: 0,
                        '& .MuiDrawer-paper': {
                            width: isSmallScreen ? smallDrawerWidth : drawerWidth,
                            boxSizing: 'border-box',
                        },
                    }}
                    variant="permanent"
                    anchor="left"
                    className='nav-user'
                >
                    <Toolbar>
                        <Box className="block-logo-img">
                            <Image
                                src={isSmallScreen ? "/image/icon-zing-user.png" : "/image/logo_zing_2024-05-28 135406.png"}
                                alt='Không có ảnh'
                                width={isSmallScreen ? 40 : 200}
                                height={isSmallScreen ? 40 : 70}
                                priority // Giúp hình ảnh được tải trước
                            />
                        </Box>
                    </Toolbar>
                    <Divider />
                    <List
                        sx={{ lineHeight: 2.5 }}
                    >
                        <Link href="/library" className='link-menu-user'>
                            <ListItemButton >
                                <ListItemIcon>
                                    <LibraryMusicIcon className='icon-list' />
                                </ListItemIcon>
                                {isSmallScreen ? smallDrawerWidth : <ListItemText primary="Thư Viện" />}
                            </ListItemButton>
                        </Link>
                        <Link href="/" className='link-menu-user'>
                            <ListItemButton >
                                <ListItemIcon>
                                    <AlbumIcon className='icon-list' />
                                </ListItemIcon>
                                {isSmallScreen ? smallDrawerWidth : <ListItemText primary="Khám Phá" />}
                            </ListItemButton>
                        </Link>
                    </List>
                </Drawer>
                <Box
                    component="main"
                    sx={{
                        flexGrow: 1, bgcolor: 'background.default', p: 5,
                        width: isSmallScreen ? smallDrawerWidth : drawerWidth,
                        flexShrink: 0,
                        '& .MuiDrawer-paper': {
                            width: isSmallScreen ? smallDrawerWidth : drawerWidth,
                            boxSizing: 'border-box',
                        },
                        paddingBottom: "100px",
                        backgroundColor: "#170F23"
                    }}
                >
                    <Toolbar />
                    {children}
                </Box>
            </Box>
        </>
    );
}
