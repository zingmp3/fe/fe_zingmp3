'use client'

import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import { useState } from "react";
import Slider, { Settings } from "react-slick";
import { Button, Grid, Typography } from "@mui/material";
import ChevronRightIcon from '@mui/icons-material/ChevronRight';
import ChevronLeftIcon from '@mui/icons-material/ChevronLeft';
import Image from "next/image";
import Link from "next/link";
import "../../../../public/css/user/discover.component.scss"
import { useTrackContext } from "@/lib/track.wrapper";
import PlayArrowIcon from '@mui/icons-material/PlayArrow';

interface IProps {
    data: ITracks[]
}

function DiscoverSub(props: IProps) {
    const { data } = props;
    const [category, setCategory] = useState('Tất Cả');
    const handleCategoryChange = (newCategory: string) => {
        setCategory(newCategory);
    };
    //  lọc thể loại nhạc
    const filteredSongs = category === 'Tất Cả' ? data : data.filter(song => song.category === category);

    function SampleNextArrow(props: any) {
        const { onClick } = props;
        return (
            <Button
                color="inherit"
                variant="contained"
                onClick={onClick}
                className="sample-next-arrow"
            >
                <ChevronRightIcon
                    sx={{
                        fontSize: "25px",
                        color: "#fff"
                    }}
                />
            </Button>
        );
    }

    function SamplePrevArrow(props: any) {
        const { onClick } = props;
        return (

            <Button
                color="inherit"
                variant="contained"
                onClick={onClick}
                className="sample-prev-arrow"
            >
                <ChevronLeftIcon
                    sx={{
                        fontSize: "25px",
                        color: "#fff",
                    }}
                />
            </Button>
        )
    }

    const settings: Settings = {
        dots: false,
        infinite: true,
        autoplay: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        nextArrow: <SampleNextArrow />,
        prevArrow: <SamplePrevArrow />,
        responsive: [
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1,
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    };

    const { currentTrack, setCurrentTrack } = useTrackContext() as ITrackContext;

    const handleListTracksPlay = (track: any) => {
        if (!track || track.length === 0) {
            return;
        }
        if (!currentTrack || currentTrack._id !== track._id) {
            setCurrentTrack({ ...track, isPlaying: true });
        } else {
            setCurrentTrack({ ...currentTrack, isPlaying: !currentTrack.isPlaying });
        }
    };

    return (
        <>
            <div className="slider-container">
                <Slider {...settings}>
                    <div key="slide1" className="img-block">
                        <Link href="/">
                            <Image
                                style={{
                                    width: "100%",
                                    height: "100%"
                                }}
                                src={`${process.env.NEXT_PUBLIC_BACKEND_URL}/slideshow/e1ca85748b4e8e9d98f2e28e9c3a6967.jpg`}
                                alt="Image Error"
                                width={100}
                                height={100}
                            />
                        </Link>
                    </div>
                    <div key="slide2" className="img-block">
                        <Link href="/">
                            <Image
                                style={{
                                    width: "100%",
                                    height: "100%"
                                }}
                                src={`${process.env.NEXT_PUBLIC_BACKEND_URL}/slideshow/466aedcacaecbcceec2391c4b36fd58c.jpg`}
                                alt="Image Error"
                                width={100}
                                height={100}
                            />
                        </Link>
                    </div>
                    <div key="slide3" className="img-block">
                        <Link href="/">
                            <Image
                                style={{
                                    width: "100%",
                                    height: "100%"
                                }}
                                src={`${process.env.NEXT_PUBLIC_BACKEND_URL}/slideshow/d2c8b5b5f0facbacbf122d8015642933.jpg`}
                                alt="Image Error"
                                width={100}
                                height={100}
                            />
                        </Link>
                    </div>

                    <div key="slide4" className="img-block">
                        <Link href="/">
                            <Image
                                style={{
                                    width: "100%",
                                    height: "100%"
                                }}
                                src={`${process.env.NEXT_PUBLIC_BACKEND_URL}/slideshow/9e549a9f53b0b9196e4d90d38acd4eef.jpg`}
                                alt="Image Error"
                                width={100}
                                height={100}
                            />
                        </Link>
                    </div>
                </Slider>
            </div>

            <div className="music-released">
                <Typography variant="h5" color={"#fff"}>
                    Mới phát hành
                </Typography>
                <div className="change-category">
                    <Button variant="outlined" disableRipple className={category === 'Tất Cả' ? "active-btn-category" : ""} onClick={() => handleCategoryChange("Tất Cả")}>Tất cả</Button>
                    <Button variant="outlined" disableRipple className={category === 'VN' ? "active-btn-category" : ""} onClick={() => handleCategoryChange("VN")}>Việt Nam</Button>
                    <Button variant="outlined" disableRipple className={category === 'QT' ? "active-btn-category" : ""} onClick={() => handleCategoryChange("QT")}>Quốc Tế</Button>
                </div>
            </div>

            <Grid>
                <Grid item xs={4} md={6} sm={12} lg={12}>
                    <div className="song-list">
                        {filteredSongs.map(song => {
                            const isListTrackPlaying = currentTrack._id === song._id && currentTrack.isPlaying;
                            return (
                                <div key={song._id} className={`song-item ${currentTrack._id === song._id ? "active-background" : ""}`}>
                                    <div className="song-img">
                                        <Image
                                            src={`${process.env.NEXT_PUBLIC_BACKEND_URL}/img-song/${song.imgUrl}`}
                                            alt={""}
                                            width={60}
                                            height={60}
                                        />
                                        <div className={`icon ${isListTrackPlaying ? "show-equalizer" : ""}`}
                                            onClick={() => handleListTracksPlay(song)}
                                        >
                                            {isListTrackPlaying ?
                                                <Image
                                                    src={"/image/Equalizer-icon-unscreen.gif"}
                                                    alt=""
                                                    width={0}
                                                    height={0}
                                                />
                                                : <PlayArrowIcon className="icon-play-track" htmlColor="#fff" fontSize="large" />
                                            }
                                        </div>
                                    </div>

                                    <div className="song-title">
                                        <Link href={`/track/${song._id}`}><h3 className="song-name">{song.title}</h3></Link>

                                        <div className="song-detail">
                                            <p>{song.singer}</p>
                                            <p>Hôm nay</p>
                                        </div>
                                    </div>

                                </div>
                            )
                        })
                        }
                    </div>
                </Grid>
            </Grid>
        </>
    );
}



export default DiscoverSub;
