'use client'

import { useState } from "react"
import Divider from "@mui/material/Divider"
import Image from "next/image"
import Link from "next/link"
import "../../../../public/css/user/search.scss"

interface IProps {
    tracks: ITracks[] | undefined
    params: string | null
}

const SearchSub = (props: IProps) => {
    const { tracks, params } = props;
    const [listTrack, setListTrack] = useState<ITracks[] | undefined>(tracks);
  
    return (
        <>
            {!listTrack?.length ? <p className="search-empty">Kết quả tim kiếm không tồn tại </p> :
                (<div>
                    <p className="search-title">Kết quả tim kiếm cho từ khóa:  {params}</p>
                    <Divider /><br />
                    {listTrack?.map((data) => {
                        return (
                            <div key={data._id} className="search-block">
                                <Image
                                    src={`${process.env.NEXT_PUBLIC_BACKEND_URL}/img-song/${data.imgUrl}`}
                                    alt=""
                                    width={80}
                                    height={80}
                                />
                                <Link className="track-name-search" href={`/track/${data._id}`}>
                                    {data.title}
                                </Link>
                            </div>
                        )
                    })}
                </div>)
            }
        </>
    )
}

export default SearchSub



