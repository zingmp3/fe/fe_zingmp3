'use client'

import { useState } from "react";
import FavoriteBorderIcon from '@mui/icons-material/FavoriteBorder';
import Tooltip, { TooltipProps, tooltipClasses } from '@mui/material/Tooltip';
import { styled } from '@mui/material/styles';
import { useSession } from "next-auth/react";
import { useRouter } from "next/navigation";
import FavoriteIcon from '@mui/icons-material/Favorite';
import Snackbar from '@mui/material/Snackbar';
import Alert from '@mui/material/Alert';
import { queryAddLikeTrack } from "@/utils/api/user/like.api";

interface IProps {
    like: ILike[],
    paramsId: string | undefined
}

const LikeTracksDetail = (props: IProps) => {
    const { like, paramsId } = props;
    const { data: session } = useSession()
    const router = useRouter()
    const [openLike, setOpenLike] = useState(false);
    const [openDisLike, setOpenDisLike] = useState(false);

    const handleClickIsLike = () => {
        setOpenLike(true);
    };

    const handleCloseIsLike = () => {
        setOpenLike(false);
    };

    const handleClickDisLike = () => {
        setOpenDisLike(true);
    };

    const handleCloseDisLike = () => {
        setOpenDisLike(false);
    };

    const TooltipIcon = styled(({ className, ...props }: TooltipProps) => (
        <Tooltip {...props} arrow classes={{ popper: className }} />
    ))(({ theme }) => ({
        [`& .${tooltipClasses.arrow}`]: {
            color: "#7D7D7D",
        },
        [`& .${tooltipClasses.tooltip}`]: {
            backgroundColor: "#7D7D7D",
        },
    }));

    const handleAddLikeTrack = async () => {
        await queryAddLikeTrack(
            like,
            session?.user._id,
            session?.access_token,
            router,
            paramsId,
        )
    }

    let isLike = like.some(data => data.tracks.some(item => item._id === paramsId))

    return (
        <>
            <TooltipIcon placement="top" title={isLike ? "Bỏ yêu thích" : "Yêu thích"}>
                {isLike ?
                    <FavoriteIcon fontSize="large" className={`icon-active ${isLike ? "hilight" : ""}`} onClick={() => {
                        handleAddLikeTrack()
                        handleClickDisLike()
                    }} />
                    :
                    <FavoriteBorderIcon fontSize="large" className={`icon-active`} onClick={() => {
                        handleAddLikeTrack()
                        handleClickIsLike()
                    }
                    } />
                }
            </TooltipIcon>

            <Snackbar
                open={openLike}
                autoHideDuration={2000}
                onClose={() => handleCloseIsLike()}
            >
                <Alert
                    style={{
                        backgroundColor: "#34224F",
                        color: "#fff",
                    }}
                    icon={false}
                    onClose={() => handleCloseIsLike()}
                    sx={{ width: '100%' }}
                >
                    Đã thêm vào danh sách yêu thích
                </Alert>
            </Snackbar >
            <Snackbar
                open={openDisLike}
                autoHideDuration={2000}
                onClose={() => handleCloseDisLike()}
            >
                <Alert
                    style={{
                        backgroundColor: "#34224F",
                        color: "#fff",
                    }}
                    icon={false}
                    onClose={() => handleCloseDisLike()}
                    sx={{ width: '100%' }}
                >
                    Đã xóa khỏi danh sách yêu thích
                </Alert>
            </Snackbar >
        </>
    )
}

export default LikeTracksDetail