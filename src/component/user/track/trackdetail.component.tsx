'use client'

import { useEffect, useState } from "react";
import { Box, TextField } from "@mui/material"
import Image from "next/image"
import Grid from '@mui/material/Grid';
import { useSearchParams } from "next/navigation"
import FavoriteBorderIcon from '@mui/icons-material/FavoriteBorder';
import PlaylistAddIcon from '@mui/icons-material/PlaylistAdd';
import "./trackdetail.comment.scss"
import PlayArrowIcon from '@mui/icons-material/PlayArrow';
import Tooltip, { TooltipProps, tooltipClasses } from '@mui/material/Tooltip';
import { styled } from '@mui/material/styles';
import { useTrackContext } from "../../../lib/track.wrapper";


interface IProps {
  track: ITracks
}

const TracksSub = (props: IProps) => {
  const { track } = props;
  const { currentTrack, setCurrentTrack } = useTrackContext() as ITrackContext;

  useEffect(() => {
    if (!currentTrack.isPlaying) {
      setCurrentTrack({ ...track, isPlaying: true });
    }
  }, [track]);

  const handleClickPlay = () => {
    if (!currentTrack || currentTrack._id !== track._id) {
      setCurrentTrack({ ...track, isPlaying: true });
    } else {
      setCurrentTrack({ ...currentTrack, isPlaying: !currentTrack.isPlaying });
    }
  };

  const isTrackPlaying = currentTrack._id === track._id && currentTrack.isPlaying;

  const TooltipIcon = styled(({ className, ...props }: TooltipProps) => (
    <Tooltip {...props} arrow classes={{ popper: className }} />
  ))(({ theme }) => ({
    [`& .${tooltipClasses.arrow}`]: {
      color: "#7D7D7D",
    },
    [`& .${tooltipClasses.tooltip}`]: {
      backgroundColor: "#7D7D7D",
    },
  }));

  return (
    <>
      <Grid container spacing={2} className="track-detail">
        <Grid item xs={12} sm={12} md={12} xl={4} >
          <Box textAlign={"center"} className="track-block">
            <div className="track-block-img">
              <div className={`track-img`} onClick={() => handleClickPlay()}>
                <Image
                  src={`${process.env.NEXT_PUBLIC_BACKEND_URL}/img-song/${track.imgUrl}`}
                  alt=""
                  width={100}
                  height={100}
                />
                <div className={`icon ${isTrackPlaying ? "show-equalizer" : ""}`}>
                  {isTrackPlaying ?
                    <Image
                      src={"/image/Equalizer-icon-unscreen.gif"}
                      alt=""
                      width={0}
                      height={0}
                    />
                    : <PlayArrowIcon className="icon-play-track" htmlColor="#fff" fontSize="large" />}
                </div>
              </div>
            </div>

            <div className="track-info">
              <div className="track-text">
                <span className="track-title">{track.title}</span><br />
                <span className="track-singer">{track.singer}</span>
              </div>

              <div className="track-active">
                <TooltipIcon placement="top" title="Yêu thích">
                  <FavoriteBorderIcon fontSize="large" className="icon-active" />
                </TooltipIcon>
                <TooltipIcon placement="top" title="Thêm vào danh sách phát">
                  <PlaylistAddIcon fontSize="large" className="icon-active" />
                </TooltipIcon>
              </div>
            </div>
          </Box>
        </Grid >

        <Grid item xs={12} sm={12} md={12} xl={8}>
          <div className="comment-block">
            <div>
              <TextField
                id="standard-basic"
                label="Comment"
                variant="standard"
                fullWidth
                sx={{
                  '& label': {
                    color: '#fff',
                  },
                  '& label.Mui-focused': {
                    color: '#fff',
                  },
                  '& .MuiInput-underline:before': {
                    borderBottomColor: '#fff',
                  },
                  '& .MuiInput-underline:after': {
                    borderBottomColor: '#fff',
                  },
                  '& .MuiInputBase-input': {
                    color: '#fff',
                  },
                  '& .css-953pxc-MuiInputBase-root-MuiInput-root:hover:not(.Mui-disabled, .Mui-error):before': {
                    borderBottom: '2px solid #fff',
                  }
                }}
              />
            </div>
          </div>
        </Grid>
      </Grid >
    </>
  )
}


export default TracksSub