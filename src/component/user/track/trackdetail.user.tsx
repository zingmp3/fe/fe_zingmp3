'use client'

import { useEffect, useState } from "react";
import { Box } from "@mui/material"
import Image from "next/image"
import Grid from '@mui/material/Grid';
import "../../../../public/css/user/trackDetail.user.scss";
import PlayArrowIcon from '@mui/icons-material/PlayArrow';
import Tooltip, { TooltipProps, tooltipClasses } from '@mui/material/Tooltip';
import { styled } from '@mui/material/styles';
import { useSession } from "next-auth/react";
import CommentTrack from "./comment.user";
import { useTrackContext } from "@/lib/track.wrapper";
import LikeTracksDetail from "./like.user";
import TracksAddPlaylist from "./trackAddPlaylist.user";


interface IProps {
  track: ITracks,
  comment: ITrackComment[],
  like: ILike[],
  paramsId: string,
  playlistDetail: IPlaylist[]
}

const TracksSub = (props: IProps) => {
  const { track, comment, like, paramsId, playlistDetail } = props;
  const { data: session } = useSession()


  const [holdOption, setHoldOption] = useState(false)


  console.log("check parrams", paramsId)
  // console.log("check sesion:", session)

  const { currentTrack, setCurrentTrack } = useTrackContext() as ITrackContext;
  // console.log("check currentTrack detail: ", currentTrack)



  const TooltipIcon = styled(({ className, ...props }: TooltipProps) => (
    <Tooltip {...props} arrow classes={{ popper: className }} />
  ))(({ theme }) => ({
    [`& .${tooltipClasses.arrow}`]: {
      color: "#7D7D7D",
    },
    [`& .${tooltipClasses.tooltip}`]: {
      backgroundColor: "#7D7D7D",
    },
  }));




  useEffect(() => {
    if (currentTrack._id === "string") {
      setCurrentTrack({ ...track });  // bỏ isPlaying khi mới vào trang để tránh xung đột (play/pause)
    }
    else if (currentTrack._id !== track._id) {
      setCurrentTrack({ ...currentTrack, isPlaying: true });
    }
  }, [track]);




  const handleClickPlay = () => {
    if (!currentTrack || currentTrack._id !== track._id) {
      setCurrentTrack({ ...track, isPlaying: true });
    } else {
      setCurrentTrack({ ...currentTrack, isPlaying: !currentTrack.isPlaying });
    }
  };

  const isTrackPlaying = currentTrack._id === track._id && currentTrack.isPlaying;




  return (
    <>
      <Grid container spacing={2} className="track-detail">

        <Grid item xs={12} sm={12} md={12} xl={4} >
          <Box textAlign={"center"} className="track-block">

            <div className="track-block-img">
              <div className={`track-img-tracks-detail`} onClick={() => handleClickPlay()}>

                <Image
                  src={`${process.env.NEXT_PUBLIC_BACKEND_URL}/img-song/${track.imgUrl}`}
                  alt=""
                  width={100}
                  height={100}
                />
                <div className={`icon ${isTrackPlaying ? "show-equalizer" : ""}`}>
                  {isTrackPlaying ?
                    <Image
                      src={"/image/Equalizer-icon-unscreen.gif"}
                      alt=""
                      width={0}
                      height={0}
                      className=""
                    />
                    : <PlayArrowIcon className="icon-play-track" htmlColor="#fff" fontSize="large" />}
                </div>

              </div>
            </div>


            <div className="track-info">
              <div className="track-text">
                <span className="track-title">{track.title}</span><br />
                <span className="track-singer">{track.singer}</span>
              </div>


              <div className="track-active">

                <LikeTracksDetail
                  like={like}
                  paramsId={paramsId}
                />


                <TracksAddPlaylist
                  setHoldOption={setHoldOption}
                  playlist={playlistDetail}
                  holdOption={holdOption}
                  idTrack={track._id}
                />
              </div>

            </div>
          </Box>
        </Grid >


        <Grid item xs={12} sm={12} md={12} xl={8}>

          <CommentTrack
            comment={comment}
          />

        </Grid>
      </Grid >


    </>
  )
}


export default TracksSub