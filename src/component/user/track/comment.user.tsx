import { TextField } from "@mui/material"
import Image from "next/image"
import "../../../../public/css/user/comment.user.scss";

import { useSession } from "next-auth/react"
import { useState } from "react"
import { useRouter } from "next/navigation"
import { useTrackContext } from "@/lib/track.wrapper"
import { queryCreateComment } from "@/utils/api/user/comment.api"

const CommentTrack = (props: any) => {
  const { comment } = props

  const { data: session } = useSession()
  const router = useRouter()
  const [contentComment, setContentComment] = useState('');
  const { currentTrack, setCurrentTrack } = useTrackContext() as ITrackContext;

  const handleCreateComment = async () => {
    await queryCreateComment(
      contentComment,
      currentTrack,
      session?.user._id,
      session?.access_token,
      setContentComment,
      router,
    )
  }

  return (
    <div className="comment-block">
      <div>
        <TextField
          id="standard-basic"
          label="Comment"
          variant="standard"
          fullWidth
          value={contentComment}
          onChange={(e) => setContentComment(e.target.value)}
          sx={{
            '& label': {
              color: '#fff',
            },
            '& label.Mui-focused': {
              color: '#fff',
            },
            '& .MuiInput-underline:before': {
              borderBottomColor: '#fff',
            },
            '& .MuiInput-underline:after': {
              borderBottomColor: '#fff',
            },
            '& .MuiInputBase-input': {
              color: '#fff',
            },
            '& .css-953pxc-MuiInputBase-root-MuiInput-root:hover:not(.Mui-disabled, .Mui-error):before': {
              borderBottom: '2px solid #fff',
            }
          }}
          onKeyDown={(e) => {
            if (e.code === "Enter") {
              handleCreateComment();
            }
          }}
        />
      </div>
      <br />
      {comment?.map((comment: ITrackComment) => (
        <div className="block-list-comment" key={comment._id}>
          <div className="img-user-comment">
            <Image
              src={"/image/profile.png"}
              alt=""
              width={50}
              height={50}
            />
          </div>
          <div className="info-comment">
            <span className="user-name">{comment.userId.email}</span>
            <h4 className="content-comment">{comment.content}</h4>
          </div>
        </div>
      ))
      }
    </div>
  )
}

export default CommentTrack;