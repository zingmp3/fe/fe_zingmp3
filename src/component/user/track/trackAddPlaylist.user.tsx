
import { useEffect, useState } from 'react';
import Menu from '@mui/material/Menu';
import PlaylistAddIcon from '@mui/icons-material/PlaylistAdd';
import Tooltip from '@mui/material/Tooltip';
import TextField from '@mui/material/TextField';
import Autocomplete from '@mui/material/Autocomplete';
import QueueMusicIcon from '@mui/icons-material/QueueMusic';
import PlayListAddModal from '../library/playlist.add';
import { useSession } from 'next-auth/react';
import "../../../../public/css/user/trackDetail.user.scss";
import { useRouter } from 'next/navigation';
import { Alert, Snackbar } from '@mui/material';
import { queryAddTrackPlaylist, queryPlaylistSearchChange } from '@/utils/api/user/track.api';

interface IProps {
    setHoldOption?: (v: boolean) => void
    playlist: IPlaylist[]
    holdOption?: boolean
    idTrack?: string | undefined
}

function TracksAddPlaylist(props: IProps) {
    const { setHoldOption, playlist, holdOption, idTrack } = props;
    const router = useRouter()
    const { data: session } = useSession()
    const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
    const [searchQuery, setSearchQuery] = useState<string>('');
    const [playlistNew, setPlaylistNew] = useState(playlist);
    const [getTitle, setGetTitle] = useState('');
    const [playlistSuggestions, setPlaylistSuggestions] = useState<IPlaylist[]>([]);
    const [initialPlaylists, setInitialPlaylists] = useState<IPlaylist[]>([]); // Lưu trữ dữ liệu ban đầu
    let open = Boolean(anchorEl);

    const handleClickOpenPlaylist = (event: React.MouseEvent<HTMLElement>) => {
        setAnchorEl(event.currentTarget);
        setHoldOption ? setHoldOption(true) : null
    };
    const handleClose = () => {
        setAnchorEl(null);
        setHoldOption ? setHoldOption(false) : null;
    };

    const [openAddPlaylist, setOpenAddPlaylist] = useState(false);
    const handleClickIsAddPlaylist = () => {
        setOpenAddPlaylist(true);
    };

    const handleCloseIsAddPlaylist = () => {
        setOpenAddPlaylist(false);
    };

    const handleSearchChange = async (event: React.ChangeEvent<HTMLInputElement>) => {
        await queryPlaylistSearchChange(
            event.target.value,
            setSearchQuery,
            session?.access_token,
            setPlaylistNew,
            setPlaylistSuggestions,
            initialPlaylists
        )
    }

    const handleAddTrackPlaylist = async (playlistItem: Object) => {
        await queryAddTrackPlaylist(
            session?.user._id,
            session?.access_token,
            router,
            idTrack,
            playlistItem
        )
    }

    useEffect(() => {
        setInitialPlaylists(playlist); // Lưu trữ dữ liệu ban đầu 
    }, [playlist]);

    return (
        <>
            <Tooltip arrow disableInteractive placement="top" title="Thêm vào Playlist">
                <div onClick={handleClickOpenPlaylist}>
                    <PlaylistAddIcon fontSize="large" className="icon-active" />
                </div>
            </Tooltip>

            <Menu
                id="basic-menu"
                anchorEl={anchorEl}
                open={open}
                onClose={handleClose}
                anchorOrigin={{
                    vertical: 'top',
                    horizontal: 'left',
                }}
                transformOrigin={{
                    vertical: 'bottom',
                    horizontal: 'right',
                }}
                className='menu-add-playlist'
            >
                <div className='content-playlist'>
                    <Autocomplete
                        open={false}
                        options={playlistSuggestions}
                        getOptionLabel={(option) => typeof option === 'string' ? option : option.title}
                        onChange={(event, newValue) => newValue}
                        sx={{ width: 260 }}
                        renderInput={(params) => (
                            <TextField
                                {...params}
                                placeholder='Tìm playlist'
                                variant="outlined"
                                onChange={handleSearchChange}
                                value={searchQuery}
                                className='search-playlist'
                            />
                        )}
                    />

                    <div className="list-playlist">
                        <div className="block-playlist">
                            <PlayListAddModal
                                holdOption={holdOption}
                            />
                            {playlistNew.map(item => {
                                return (
                                    <div className="item-playlist" key={item._id} onClick={() => {
                                        // console.log("check itemId:", item)
                                        handleAddTrackPlaylist(item);
                                        handleClickIsAddPlaylist();
                                        setGetTitle(item.title)
                                        setAnchorEl(null);
                                    }
                                    }>
                                        <span className="icon-child-playlist"><QueueMusicIcon /></span>
                                        <span className="child-playlist"> {item.title}</span>
                                    </div>
                                )
                            })
                            }
                        </div>
                    </div>
                </div>
            </Menu>

            <Snackbar
                open={openAddPlaylist}
                autoHideDuration={2000}
                onClose={() => handleCloseIsAddPlaylist()}
            >
                <Alert
                    style={{
                        backgroundColor: "#34224F",
                        color: "#fff",
                    }}
                    icon={false}
                    onClose={() => handleCloseIsAddPlaylist()}
                    sx={{ width: '100%' }}
                >
                    Đã thêm nhạc vào danh sách thư viện {getTitle}
                </Alert>
            </Snackbar >
        </>
    );
}

export default TracksAddPlaylist
