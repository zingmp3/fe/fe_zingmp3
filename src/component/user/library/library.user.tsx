'use client'

import "../../../../public/css/user/library.user.scss";
import { useState } from 'react';
import { useTrackContext } from '@/lib/track.wrapper';
import PlaylistLibrary from './playlist/playlist.user';
import LikeUserChild from "./like/like_chidrent";

interface IProps {
    playlist: IPlaylist[],
    like: ILike[]
}

const LibrarySub = (props: IProps) => {
    const { playlist, like } = props;

    const flag = true;
    const [holdOption, setHoldOption] = useState(false)
    const { currentTrack, setCurrentTrack } = useTrackContext() as ITrackContext;
    const [selectedTrackId, setSelectedTrackId] = useState<string | undefined>('');

    const handleHoldTrackClick = (trackId: string | undefined) => {
        setSelectedTrackId(trackId);
    };

    const handleClickPlay = (track: any) => {
        if (!track || track.length === 0) {
            return;
        }
        if (!currentTrack || currentTrack._id !== track[0]._id) {
            setCurrentTrack({ ...track[0], isPlaying: true });
        } else {
            setCurrentTrack({ ...currentTrack, isPlaying: !currentTrack.isPlaying });
        }
    }

    const handleListTracksPlay = (track: any) => {
        if (!track || track.length === 0) {
            return;
        }
        if (!currentTrack || currentTrack._id !== track._id) {
            setCurrentTrack({ ...track, isPlaying: true });
        } else {
            setCurrentTrack({ ...currentTrack, isPlaying: !currentTrack.isPlaying });
        }
    };

    return (
        <>
            <PlaylistLibrary
                playlist={playlist}
                flag={flag}
                handleClickPlay={handleClickPlay}

            />
            <LikeUserChild
                like={like}
                playlist={playlist}
                holdOption={holdOption}
                setHoldOption={setHoldOption}
                selectedTrackId={selectedTrackId}
                handleHoldTrackClick={handleHoldTrackClick}
                handleListTracksPlay={handleListTracksPlay}
            />
        </>
    )
}

export default LibrarySub