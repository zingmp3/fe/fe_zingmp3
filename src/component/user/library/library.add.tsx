'use client'

import { useState } from 'react';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import Modal from '@mui/material/Modal';
import "./library.user.scss"
import AddIcon from '@mui/icons-material/Add';
import { TextField, Tooltip, TooltipProps, styled, tooltipClasses } from '@mui/material';
import { useSession } from 'next-auth/react';
import { useRouter } from 'next/navigation';
import CloseIcon from '@mui/icons-material/Close';
import { sendRequest } from '@/utils/api';


const PlayListAddModal = () => {

    const [open, setOpen] = useState(false);
    const handleOpen = () => setOpen(true);
    const handleClose = () => setOpen(false);
    const { data: session } = useSession()
    const router = useRouter()
    const [titlePlaylist, setTitlePlaylist] = useState('');

    const TooltipIcon = styled(({ className, ...props }: TooltipProps) => (
        <Tooltip {...props} arrow classes={{ popper: className }} />
    ))(({ theme }) => ({
        [`& .${tooltipClasses.arrow}`]: {
            color: "#7D7D7D",
        },
        [`& .${tooltipClasses.tooltip}`]: {
            backgroundColor: "#7D7D7D",
        },
    }));

    const handleAddPlaylist = async () => {
        const playlist = await sendRequest<IModelPaginate<IPlaylist>>({
            url: "http://localhost:8000/playlist",
            method: "POST",
            body: {
                title: titlePlaylist,
                userId: session?.user._id
            },
            headers: {
                'Authorization': `Bearer ${session?.access_token}`,
            },
        })
        if (playlist) {
            await sendRequest<IBackendRes<any>>({  //  clear data cache
                url: `/api/revalidate`,
                method: "POST",
                queryParams: {
                    tag: "fetch-playlist",
                }
            })
            router.refresh();  //  clear router cache 
            handleClose()
            setTitlePlaylist("")
        }
    }

    return (
        <>
            <TooltipIcon placement='top' title="Thêm Playlist mới">
                <AddIcon
                    className='icon-add-playlist'
                    onClick={handleOpen}
                />
            </TooltipIcon>
            <Modal
                open={open}
                onClose={handleClose}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box className={"modal-add-playlist"}>
                    <Box className="block-close-playlist">
                        <CloseIcon className='close-model-playlist' onClick={handleClose} />
                    </Box>
                    <Typography id="modal-modal-title" color={"#fff"} fontWeight={'600'} variant="h6" component="h2">
                        Tạo playlist mới
                    </Typography>
                    <TextField
                        placeholder='Nhập tên Playlist'
                        className='field-add-playlist'
                        fullWidth
                        value={titlePlaylist}
                        onChange={(e) => setTitlePlaylist(e.target.value)}
                        onKeyDown={(e) => {
                            if (e.code === "Enter") {
                                handleAddPlaylist()
                            }
                        }}
                    />
                    <br />

                    {!titlePlaylist.length ?
                        <Button
                            className={`btn-create-playlist  ${!titlePlaylist.length ? 'drop' : ""}`}
                            fullWidth
                            variant="outlined"
                            disableRipple
                        >
                            Tạo mới
                        </Button>
                        :
                        <Button
                            className={`btn-create-playlist`}
                            fullWidth
                            variant="outlined"
                            disableRipple
                            onClick={() =>
                                handleAddPlaylist()
                            }
                        >
                            Tạo mới
                        </Button>
                    }
                </Box>
            </Modal>
        </>
    );
}

export default PlayListAddModal