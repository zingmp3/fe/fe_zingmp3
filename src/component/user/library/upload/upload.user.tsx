'use client'

import { useCallback } from 'react';
import { useTrackContext } from '@/lib/track.wrapper';
import PlaylistLibrary from "../playlist/playlist.user";
import Link from 'next/link';
import { Button } from '@mui/material';
import { styled } from '@mui/material/styles';
import CloudUploadIcon from '@mui/icons-material/CloudUpload';
import "../../../../../public/css/user/upload.user.scss";
import { useSession } from 'next-auth/react';
import { FileWithPath, useDropzone } from 'react-dropzone';
import Image from 'next/image';
import PlayArrowIcon from '@mui/icons-material/PlayArrow';
import { useRouter } from 'next/navigation';
import DeleteTrackPlaylist from '../detail_playlist/deleteTrackPlaylist';
import { queryUploadFileTrack } from '@/utils/api/user/upload.api';

interface IProps {
    playlist: IPlaylist[],
    uploads: any | null
}

const VisuallyHiddenInput = styled('input')({
    clip: 'rect(0 0 0 0)',
    clipPath: 'inset(50%)',
    height: 1,
    overflow: 'hidden',
    position: 'absolute',
    bottom: 0,
    left: 0,
    whiteSpace: 'nowrap',
    width: 1,
});



const InputComponentFile = () => {
    return (
        <Button
            disableRipple
            tabIndex={-1}
            variant='outlined'
            startIcon={<CloudUploadIcon className='icon-upload' />}
            onClick={(event) => event.preventDefault()}
            className='btn-upload'
        >
            Đã tải lên
            <VisuallyHiddenInput type="file" />
        </Button>
    )
}

const UploadUserSub = (props: IProps) => {
    const { playlist, uploads } = props;
    const flag = true;
    const { currentTrack, setCurrentTrack } = useTrackContext() as ITrackContext;
    let isUpload = true
    const router = useRouter()

    const { data: session } = useSession()

    const handleClickPlay = (track: any) => {
        if (!track || track.length === 0) {
            return;
        }
        if (!currentTrack || currentTrack._id !== track[0]._id) {
            setCurrentTrack({ ...track[0], isPlaying: true });
        } else {
            setCurrentTrack({ ...currentTrack, isPlaying: !currentTrack.isPlaying });
        }
    }

    const onDrop = useCallback(async (acceptedFiles: FileWithPath[]) => {
        if (acceptedFiles && acceptedFiles[0]) {
            const audio = acceptedFiles[0];
            const formData = new FormData();
            formData.append("fileUpload", audio);
            try {
                await queryUploadFileTrack(
                    formData,
                    session?.access_token,
                    router
                )
            } catch (error) {
                console.error("Failed to upload file:", error);
            }
        }
    }, [session])

    const { getRootProps, getInputProps } = useDropzone({
        onDrop,
        accept: {
            'audio': [".mp3", '.mp4', '.m4a', '.wav'],
        }
    })

    const handleListTracksPlay = (fileTrack: string) => {

        if (currentTrack.trackUrl === fileTrack) {
            setCurrentTrack({ ...currentTrack, isPlaying: !currentTrack.isPlaying });
        } else {
            setCurrentTrack({
                ...currentTrack,
                _id: fileTrack,
                title: fileTrack,
                singer: fileTrack,
                imgUrl: "/image/upload-image.png",
                trackUrl: fileTrack,
                isPlaying: true
            })
        }
    }

    return (
        <>

            <PlaylistLibrary
                playlist={playlist}
                flag={flag}
                handleClickPlay={handleClickPlay}
            />
            <div className="like-list-all" >
                <div className="change-option">
                    <Link href={"/library/like"}><Button variant="outlined" disableRipple>Yêu thích</Button></Link>
                    <Link href={"/library/upload"}><Button variant="outlined" className='hilight' disableRipple>Đã tải lên</Button></Link>
                </div>

                <div className="song-list-like">
                    <div className="upload-block" color='#fff'>
                        <section className="container" color='#fff'>
                            <div {...getRootProps({ className: 'dropzone' })}>
                                <input {...getInputProps()} />
                                <InputComponentFile />
                                <p>Kéo thả file vào đây hoặc click để chọn file</p>
                            </div>
                        </section>

                        {uploads.length === 0 ?
                            <div className="empty-upload">
                                <Image
                                    src={"/image/music-cloud.png"}
                                    alt=''
                                    width={100}
                                    height={100}
                                />
                                <h4>Chưa có bài hát tải lên trong thư viện cá nhân</h4>
                            </div>
                            :
                            <div className="upload-track-block">
                                <h4 className='title-upload-track'>Danh sách File Upload </h4>

                                {uploads.map((up: any) => {
                                    const isPlaying = currentTrack.trackUrl === up.filename && currentTrack.isPlaying;
                                    return (
                                        <div key={up.filename} className={`song-like-item ${currentTrack.trackUrl === up.filename ? "active-background-like" : ""}`}>
                                            <div className={`song-like-item `}>
                                                <div className="song-like-info">

                                                    <div className="song-like-img">
                                                        <Image
                                                            src={"/image/upload-image.png"}
                                                            alt={""}
                                                            width={60}
                                                            height={60}
                                                        />
                                                        <div className={`icon ${isPlaying ? "show-equalizer" : ""}`}
                                                            onClick={() =>
                                                                handleListTracksPlay ? handleListTracksPlay(up.filename) : null
                                                            }
                                                        >
                                                            {isPlaying ?
                                                                <Image
                                                                    src={"/image/Equalizer-icon-unscreen.gif"}
                                                                    alt=""
                                                                    width={0}
                                                                    height={0}
                                                                />
                                                                : <PlayArrowIcon className="icon-play-track" htmlColor="#fff" fontSize="large" />
                                                            }
                                                        </div>
                                                    </div>
                                                    <div className="song-title">
                                                        <h3 className="song-name">{up.filename}</h3>
                                                        <div className="song-detail">
                                                            <p>{up.filename}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div >
                                            <div className={`track-active-option ${currentTrack.trackUrl === up.filename ? 'show-option' : ""}`}>
                                                <DeleteTrackPlaylist
                                                    playlist={playlist}
                                                    isUpload={isUpload}
                                                    up={up.filename}
                                                />
                                            </div>
                                        </div >
                                    )
                                })
                                }
                            </div>
                        }
                    </div>
                </div>
            </div>
        </>
    )
}

export default UploadUserSub