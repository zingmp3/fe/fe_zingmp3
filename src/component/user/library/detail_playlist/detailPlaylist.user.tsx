'use client'

import { useState } from "react"
import { Box, Grid, Menu, Tooltip } from "@mui/material"
import Image from "next/image"
import Link from "next/link"
import "../../../../../public/css/user/detailPlaylist.user.scss";
import MoreHorizIcon from '@mui/icons-material/MoreHoriz';
import PlayListEditModal from "./editDetailPlaylis"
import PlayListDeleteModal from "./deleteDetailPlaylis"
import MusicOffIcon from '@mui/icons-material/MusicOff';
import { useTrackContext } from "@/lib/track.wrapper"
import PlayArrowIcon from '@mui/icons-material/PlayArrow';
import LikeTracksDetail from "../../track/like.user"
import DeleteTrackPlaylist from "./deleteTrackPlaylist"

interface IProps {
    playlist: IPlaylist
    playlistDetail: IPlaylist[]
    like: ILike[]
}

const DetailPlaylistSub = (props: IProps) => {
    const {
        playlist,
        like,
    } = props;

    const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
    const open = Boolean(anchorEl);
    const handleClick = (event: React.MouseEvent<HTMLElement>) => {
        setAnchorEl(event.currentTarget);
    };
    const handleCloseMenu = () => {
        setAnchorEl(null);
    };

    const [holdOption, setHoldOption] = useState(false)

    const [selectedTrackId, setSelectedTrackId] = useState<string | undefined>('');

    const handleHoldTrackClick = (trackId: string | undefined) => {
        setSelectedTrackId(trackId);
    };
    const { currentTrack, setCurrentTrack } = useTrackContext() as ITrackContext;

    const handleClickPlay = () => {
        if (!playlist.tracks || playlist.tracks.length < 1) {
            return;
        }
        if (currentTrack._id === "string") {
            setCurrentTrack({ ...playlist.tracks[0], isPlaying: true });
        } else {
            setCurrentTrack({ ...currentTrack, isPlaying: !currentTrack.isPlaying });
        }
    };

    const handleListTracksPlay = (track: any) => {
        if (!track || track.length === 0) {
            return;
        }
        if (!currentTrack || currentTrack._id !== track._id) {
            setCurrentTrack({ ...track, isPlaying: true });
        } else {
            setCurrentTrack({ ...currentTrack, isPlaying: !currentTrack.isPlaying });
        }
    };

    const playlistId = playlist.tracks.map(item => item._id);

    return (
        <>
            <Grid container spacing={2}>
                <Grid item xs={12} sm={12} md={12} xl={3} >
                    <div className="detail-playlist-block">`
                        <div className="track-block-img">
                            <div className={`track-img-detail`}>
                                <Image
                                    src={
                                        !playlist.tracks.length
                                            ?
                                            `${process.env.NEXT_PUBLIC_BACKEND_URL}/playlist/playlist_default.png`
                                            :
                                            `${process.env.NEXT_PUBLIC_BACKEND_URL}/img-song/${playlist.tracks[0].imgUrl}`}
                                    alt={""}
                                    width={250}
                                    height={250}
                                />
                                <div className={`icon ${playlistId.includes(currentTrack._id) && currentTrack.isPlaying ? "show-equalizer" : ""}`}
                                    onClick={() => handleClickPlay()}
                                >
                                    {playlistId.includes(currentTrack._id) && currentTrack.isPlaying ?
                                        <Image
                                            src={"/image/Equalizer-icon-unscreen.gif"}
                                            alt=""
                                            width={0}
                                            height={0}
                                            className="img-run"
                                        />
                                        : <PlayArrowIcon className="icon-play-track" htmlColor="#fff" fontSize="large" />}
                                </div>
                            </div>
                        </div>

                        <div className="detail-playlist-info">
                            <h3 className="title-playlist">{playlist.title}</h3>
                            <p className="user-create-playlist">Được tạo bởi: <span className="user-name-playlist">{playlist.userId.email}</span> </p>
                            <Tooltip arrow disableInteractive placement="top" title="Khác">
                                <div onClick={handleClick} style={{ display: 'inline-block' }}>
                                    <MoreHorizIcon className="icon-option-other" />
                                </div>
                            </Tooltip>
                            <Menu
                                anchorEl={anchorEl}
                                open={open}
                                onClose={handleCloseMenu}
                                className="option-menu"
                            >
                                <PlayListEditModal
                                    playlist={playlist}
                                    handleCloseMenu={handleCloseMenu}
                                />
                                <PlayListDeleteModal playlist={playlist} />
                            </Menu>

                        </div>
                    </div>
                </Grid>

                <Grid item xs={12} sm={12} md={12} xl={9} >
                    <div className="row-title">
                        <p className="column-name">BÀI HÁT</p>
                    </div>
                    {playlist.tracks && playlist.tracks.length > 0 ? (
                        playlist.tracks.map(track => {
                            const isListTrackPlaying = currentTrack._id === track._id && currentTrack.isPlaying;
                            return (
                                <Box className={`track-in-playlist ${currentTrack._id === track._id ? "active-background" : ""}`} key={track._id}>
                                    <Box key={track._id} className={`song-item `}>
                                        <div className="song-img">
                                            <Image
                                                src={`${process.env.NEXT_PUBLIC_BACKEND_URL}/img-song/${track.imgUrl}`}
                                                alt=""
                                                width={60}
                                                height={60}
                                            />
                                            <div className={`icon ${isListTrackPlaying && currentTrack.isPlaying ? "show-equalizer" : ""}`}
                                                onClick={() => handleListTracksPlay(track)}
                                            >
                                                {isListTrackPlaying ?
                                                    <Image
                                                        src={"/image/Equalizer-icon-unscreen.gif"}
                                                        alt=""
                                                        width={0}
                                                        height={0}
                                                    />
                                                    :
                                                    <PlayArrowIcon className="icon-play-track" htmlColor="#fff" fontSize="large" />
                                                }
                                            </div>
                                        </div>
                                        <div className="song-title">
                                            <Link href={`/track/${track._id}?audio=${track.trackUrl}`}>
                                                <h3 className="song-name">{track.title}</h3>
                                            </Link>
                                            <div className="song-detail">
                                                <p>{track.singer}</p>
                                            </div>
                                        </div>
                                    </Box>
                                    <div className={`track-active-option ${holdOption && selectedTrackId === track._id ? 'show-option' : ""}`}
                                        onClick={() =>
                                            handleHoldTrackClick ? handleHoldTrackClick(track._id) : null
                                        }
                                    >
                                        <LikeTracksDetail
                                            like={like}
                                            paramsId={track._id}
                                        />
                                        <DeleteTrackPlaylist
                                            playlist={playlist}
                                            paramsId={track._id}
                                        />
                                    </div>
                                </Box>
                            )
                        })
                    ) : (
                        <Box className="track-empty">
                            <MusicOffIcon className="track-empty-icon" />
                            <h4 className="track-empty-text">Không có bài hát trong playlist của bạn</h4>
                        </Box>
                    )}
                </Grid>
            </Grid>
        </>
    )
}

export default DetailPlaylistSub






