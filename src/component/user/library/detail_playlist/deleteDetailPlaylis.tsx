'use client'

import { useState } from 'react';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import Modal from '@mui/material/Modal';
import "../../../../../public/css/user/detailPlaylist.user.scss";
import DeleteOutlineIcon from '@mui/icons-material/DeleteOutline';
import { MenuItem, Tooltip, TooltipProps, styled, tooltipClasses } from '@mui/material';
import { useSession } from 'next-auth/react';
import { useRouter } from 'next/navigation';
import CloseIcon from '@mui/icons-material/Close';
import { queryDeletePlaylist } from '@/utils/api/user/detailPlaylist.api';

interface IProps {
    playlist: IPlaylist
    flag?: boolean
    playlistId?: string
}

const PlayListDeleteModal = (props: IProps) => {
    const { playlist, flag, playlistId } = props;

    const [openDelete, setOpenDelete] = useState(false);
    const handleOpenDelete = () => setOpenDelete(true);
    const handleCloseDelete = () => setOpenDelete(false);

    const { data: session } = useSession()
    const router = useRouter()


    const TooltipIcon = styled(({ className, ...props }: TooltipProps) => (
        <Tooltip {...props} arrow classes={{ popper: className }} />
    ))(({ theme }) => ({
        [`& .${tooltipClasses.arrow}`]: {
            color: "#000",
        },
        [`& .${tooltipClasses.tooltip}`]: {
            backgroundColor: "#000",

        },
    }));
    const handleDeletePlaylist = async () => {
        await queryDeletePlaylist(
            playlist._id,
            session?.access_token,
            router,
            handleCloseDelete,
        )
    }

    return (
        <>
            {!flag ?
                <MenuItem className="option-menu-item" onClick={handleOpenDelete}><DeleteOutlineIcon /> Xóa Playlist</MenuItem>
                :
                <TooltipIcon
                    title="Xóa"
                    placement="top"
                >
                    <Box sx={{ zIndex: 2 }} onClick={handleOpenDelete}><DeleteOutlineIcon /></Box>
                </TooltipIcon>
            }
            <Modal
                open={openDelete}
                onClose={handleCloseDelete}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box className={"modal-delete-playlist"}>
                    <Box className="block-close-playlist">
                        <CloseIcon className='close-model-playlist' onClick={handleCloseDelete} />
                    </Box>
                    <Typography id="modal-modal-title" color={"#fff"} fontWeight={'600'} variant="h6" component="h2">
                        Xóa playlist
                    </Typography>

                    <Typography color={"#fff"} variant="h6" fontSize={'13px'} component="h5">
                        Playlist của bạn sẽ bị xóa khỏi thư viện cá nhân. Bạn có muốn xóa?
                    </Typography>

                    <Box className="btn-block">
                        <Button
                            className={`btn-delete-no-playlist`}
                            fullWidth
                            variant="outlined"
                            disableRipple
                            onClick={() =>
                                handleCloseDelete()
                            }
                        >
                            Không
                        </Button>
                        <Button
                            className={`btn-delete-playlist`}
                            fullWidth
                            variant="outlined"
                            disableRipple
                            onClick={() =>
                                handleDeletePlaylist()
                            }
                        >
                            Có
                        </Button>
                    </Box>
                </Box>
            </Modal>
        </>
    );
}

export default PlayListDeleteModal