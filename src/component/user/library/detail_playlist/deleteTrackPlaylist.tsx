
import { sendRequest } from '@/utils/api';
import DeleteIcon from '@mui/icons-material/Delete';
import { styled, Tooltip, tooltipClasses, TooltipProps } from '@mui/material';
import { useSession } from 'next-auth/react';
import { useRouter } from 'next/navigation';
import { useState } from 'react';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import Modal from '@mui/material/Modal';
import CloseIcon from '@mui/icons-material/Close';
import { queryDeleteTrackOnPlaylist } from '@/utils/api/user/detailPlaylist.api';
import { queryDeleteTrackUpload } from '@/utils/api/user/upload.api';

interface IProps {
    playlist: any,
    paramsId?: string | undefined,
    isUpload?: any,
    up?: string,
}

const DeleteTrackPlaylist = (props: IProps) => {
    const {
        playlist,
        paramsId,
        isUpload,
        up
    } = props;
    const { data: session } = useSession()
    const router = useRouter()
    const [openDelete, setOpenDelete] = useState(false);
    const handleOpenDelete = () => setOpenDelete(true);
    const handleCloseDelete = () => setOpenDelete(false);

    const TooltipIcon = styled(({ className, ...props }: TooltipProps) => (
        <Tooltip {...props} arrow classes={{ popper: className }} />
    ))(({ theme }) => ({
        [`& .${tooltipClasses.arrow}`]: {
            color: "#7D7D7D",
        },
        [`& .${tooltipClasses.tooltip}`]: {
            backgroundColor: "#7D7D7D",
        },
    }));

    const handleDeleteTrackUpload = async (filename: string | undefined,) => {
        try {
            await queryDeleteTrackUpload(
                filename,
                session?.access_token,
                router
            )
        } catch (error) {
            console.error("Failed to delete file:", error);
        }
    };

    const handleDeleteTrack = async () => {
        await queryDeleteTrackOnPlaylist(
            playlist,
            paramsId,
            session?.user._id,
            session?.access_token,
            router
        );
    }

    return (
        <>
            <div>
                <TooltipIcon
                    title="Xóa khỏi Playlist"
                    placement="top"
                >
                    <div className="deleteTrackIcon" onClick={handleOpenDelete}>
                        <DeleteIcon className='icon-delete-track' />
                    </div>
                </TooltipIcon>
                <Modal
                    open={openDelete}
                    onClose={handleCloseDelete}
                    aria-labelledby="modal-modal-title"
                    aria-describedby="modal-modal-description"
                >
                    <Box className={"modal-delete-playlist"}>
                        <Box className="block-close-playlist">
                            <CloseIcon className='close-model-playlist' onClick={handleCloseDelete} />
                        </Box>
                        <Typography id="modal-modal-title" color={"#fff"} fontWeight={'600'} variant="h6" component="h2">
                            Xóa bài hát
                        </Typography>

                        <Typography color={"#fff"} variant="h6" fontSize={'13px'} component="h5">
                            {
                                isUpload ?
                                    "Bài hát sẽ bị xóa khỏi danh sách Upload. Bạn có muốn xóa?"
                                    :
                                    "Bài hát sẽ bị xóa khỏi Playlist. Bạn có muốn xóa?"
                            }
                        </Typography>
                        <Box className="btn-block">
                            <Button
                                className={`btn-delete-no-playlist`}
                                fullWidth
                                variant="outlined"
                                disableRipple
                                onClick={() =>
                                    handleCloseDelete()
                                }
                            >
                                Không
                            </Button>
                            <Button
                                className={`btn-delete-playlist`}
                                fullWidth
                                variant="outlined"
                                disableRipple
                                onClick={() =>
                                    isUpload ?
                                        handleDeleteTrackUpload(up)
                                        :
                                        handleDeleteTrack()
                                }
                            >
                                Có
                            </Button>
                        </Box>
                    </Box>
                </Modal>
            </div>
        </>
    )
}

export default DeleteTrackPlaylist