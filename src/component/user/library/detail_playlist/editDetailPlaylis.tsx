
'use client'

import { useEffect, useState } from 'react';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import Modal from '@mui/material/Modal';
import "../../../../../public/css/user/detailPlaylist.user.scss";
import { MenuItem, TextField, Tooltip, TooltipProps, styled, tooltipClasses } from '@mui/material';
import { useSession } from 'next-auth/react';
import { useRouter } from 'next/navigation';
import CloseIcon from '@mui/icons-material/Close';
import EditIcon from '@mui/icons-material/Edit';
import { queryEditPlaylist } from '@/utils/api/user/detailPlaylist.api';

interface IProps {
    playlist: IPlaylist
    handleCloseMenu?: (v: boolean) => void
    flag?: boolean
}

const PlayListEditModal = (props: IProps) => {
    const {
        playlist,
        handleCloseMenu,
        flag
    } = props;

    const [open, setOpen] = useState(false);
    const [titlePlaylist, setTitlePlaylist] = useState(playlist.title);
    const handleOpen = () => setOpen(true);
    const handleClose = () => setOpen(false);

    const { data: session } = useSession()
    const router = useRouter()

    const TooltipIcon = styled(({ className, ...props }: TooltipProps) => (
        <Tooltip {...props} arrow classes={{ popper: className }} />
    ))(({ theme }) => ({
        [`& .${tooltipClasses.arrow}`]: {
            color: "#000",
        },
        [`& .${tooltipClasses.tooltip}`]: {
            backgroundColor: "#000",
        },
    }));

    const handleEditPlaylist = async () => {
        await queryEditPlaylist(
            playlist._id,
            titlePlaylist,
            session?.user._id,
            session?.access_token,
            router,
            handleClose,
            handleCloseMenu ?? (() => { }),  //  TS yêu cầu props handleCloseMenu phải là một hàm (v: boolean) => void, nhưng vì handleCloseMenu có thể là undefined nên lỗi  ( nếu handleCloseMenu là undefined, một hàm rỗng sẽ được thay thế (() => {}), đảm bảo nó luôn nhận được một hàm như yêu cầu)
            setTitlePlaylist,
        )
    }

    useEffect(() => {
        // useState không được thay đổi tự động dựa trên thay đổi của props, nên dùng useEffect để set lại giá trị mới khi thay đổi
        setTitlePlaylist(playlist.title);
    }, [playlist]);

    return (
        <>
            {!flag ?
                <MenuItem className="option-menu-item" onClick={handleOpen}><EditIcon /> Chỉnh sửa Playlist</MenuItem>
                :
                <TooltipIcon
                    title="Chỉnh sửa"
                    placement="top"
                >
                    <Box onClick={handleOpen}><EditIcon /></Box>
                </TooltipIcon>
            }
            <Modal
                open={open}
                onClose={handleClose}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box className={"modal-add-playlist"}>
                    <Box className="block-close-playlist">
                        <CloseIcon className='close-model-playlist' onClick={handleClose} />
                    </Box>
                    <Typography id="modal-modal-title" color={"#fff"} fontWeight={'600'} variant="h6" component="h2">
                        Chỉnh sửa playlist
                    </Typography>
                    <TextField
                        placeholder='Nhập tên Playlist'
                        className='field-add-playlist'
                        fullWidth
                        value={titlePlaylist}
                        onChange={(e) => setTitlePlaylist(e.target.value)}
                        onKeyDown={(e) => {
                            if (e.code === "Enter") {
                                handleEditPlaylist()
                            }
                        }}
                    />
                    <br />

                    {!titlePlaylist.length ?
                        <Button
                            className={`btn-create-playlist  ${!titlePlaylist.length ? 'drop' : ""}`}
                            fullWidth
                            variant="outlined"
                            disableRipple
                        >
                            LƯU
                        </Button>
                        :
                        <Button
                            className={`btn-create-playlist`}
                            fullWidth
                            variant="outlined"
                            disableRipple
                            onClick={() =>
                                handleEditPlaylist()
                            }
                        >
                            LƯU
                        </Button>
                    }
                </Box>
            </Modal>
        </>
    );
}

export default PlayListEditModal