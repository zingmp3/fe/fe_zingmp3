'use client'

import Image from 'next/image';
import Link from 'next/link';
import PlayListAddModal from '../playlist.add';
import { useTrackContext } from '@/lib/track.wrapper';
import PlayArrowIcon from '@mui/icons-material/PlayArrow';
import PlayListDeleteModal from '../detail_playlist/deleteDetailPlaylis';
import PlayListEditModal from '../detail_playlist/editDetailPlaylis';

interface IProps {
    playlist: IPlaylist[],
    flag: boolean
    handleClickPlay: (v: any) => void
}

const PlaylistLibrary = (props: IProps) => {

    const {
        playlist,
        flag,
        handleClickPlay,
    } = props;

    const { currentTrack, setCurrentTrack } = useTrackContext() as ITrackContext;

    return (
        <>
            <div className="playlist-block">
                <div className="playlist-name">
                    <h2 className="title-playlist">PLAYLIST </h2>
                    <PlayListAddModal />
                </div>
                <br />
                <div className="playlist-all-item">

                    {playlist.map(data => {
                        const firstTrackId = data.tracks.length > 0 ? data.tracks[0]._id : null;
                        const isTrackPlaying = firstTrackId && currentTrack._id === firstTrackId && currentTrack.isPlaying;

                        return (
                            <div className="playlist-item" key={data._id}>
                                <div className="playlist-track-block-img">
                                    <div className={`playlist-track-img`}>
                                        <Link href={`/library/detail_playlist/${data._id}`}>
                                            <div className="playlist-img-block">
                                                <Image
                                                    src={
                                                        !data.tracks.length
                                                            ?
                                                            `${process.env.NEXT_PUBLIC_BACKEND_URL}/playlist/playlist_default.png`
                                                            :
                                                            `${process.env.NEXT_PUBLIC_BACKEND_URL}/img-song/${data.tracks[0].imgUrl}`}
                                                    alt={""}
                                                    width={250}
                                                    height={250}
                                                    className='playlist-represent-img'
                                                />
                                            </div>
                                        </Link>
                                        <div className={`icon ${isTrackPlaying && currentTrack.isPlaying ? "show-equalizer" : ""}`} onClick={() => handleClickPlay(data.tracks)}>
                                            {isTrackPlaying ?
                                                <div className="operation-playlist">
                                                    {/*  stopPropagation() chặn sự kiện click trên modal lan truyền đến phần tử cha (div.icon), do đó ngăn chặn việc gọi hàm handleClickPlay */}
                                                    <div onClick={(e) => e.stopPropagation()}>
                                                        <PlayListDeleteModal
                                                            playlist={data}
                                                            flag={flag}
                                                        />
                                                    </div>
                                                    <Image
                                                        src={"/image/Equalizer-icon-unscreen.gif"}
                                                        alt=""
                                                        width={0}
                                                        height={0}
                                                        className='img-run'
                                                    />
                                                    <div onClick={(e) => e.stopPropagation()}>
                                                        <PlayListEditModal
                                                            playlist={data}
                                                            flag={flag}
                                                        />
                                                    </div>
                                                </div>
                                                :
                                                <div className="operation-playlist">
                                                    <div onClick={(e) => e.stopPropagation()}>
                                                        <PlayListDeleteModal
                                                            playlist={data}
                                                            flag={flag}
                                                        />
                                                    </div>
                                                    <PlayArrowIcon className="icon-play-track" htmlColor="#fff" fontSize="large" />
                                                    <div onClick={(e) => e.stopPropagation()}>
                                                        <PlayListEditModal
                                                            playlist={data}
                                                            flag={flag}
                                                        />
                                                    </div>
                                                </div>
                                            }
                                        </div>
                                    </div>
                                </div>
                                <h4 className="title-playlist">{data.title}</h4>
                                <p className="user-create-playlist">{data.userId.email}</p>
                            </div>
                        )
                    })
                    }
                </div>
            </div>
            <br />
            <br />
            <hr />
            <br />

        </>
    )
}

export default PlaylistLibrary