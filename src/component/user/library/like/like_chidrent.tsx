'use client'

import Image from "next/image";
import Link from "next/link";
import "../../../../../public/css/user/like.user.scss";
import { Button } from "@mui/material";
import { useTrackContext } from "@/lib/track.wrapper";
import LikeTracksDetail from "../../track/like.user";
import TracksAddPlaylist from "../../track/trackAddPlaylist.user";
import PlayArrowIcon from '@mui/icons-material/PlayArrow';

interface IProps {
    playlist: IPlaylist[],
    like: ILike[]
    holdOption?: boolean
    setHoldOption?: (v: boolean) => void
    selectedTrackId?: string | undefined
    handleHoldTrackClick?: (v: string | undefined) => void
    handleListTracksPlay?: (v: any) => void
}

const LikeUserChild = (props: IProps) => {
    const {
        playlist,
        like,
        holdOption,
        setHoldOption,
        selectedTrackId,
        handleHoldTrackClick,
        handleListTracksPlay,
    } = props;

    const { currentTrack, setCurrentTrack } = useTrackContext() as ITrackContext;

    return (
        <>
            <div className="like-list-all">
                <div className="change-option">
                    <Link href={"/library/like"}><Button className='hilight' variant="outlined" disableRipple>Yêu thích</Button></Link>
                    <Link href={"/library/upload"}><Button variant="outlined" disableRipple>Đã tải lên</Button></Link>
                </div>

                <div className="song-list-like">
                    {like.map(song => (
                        song.tracks.map(track => {
                            const isListTrackPlaying = currentTrack._id === track._id && currentTrack.isPlaying;
                            console.log("check track", track)
                            return (
                                <div key={track._id} className={`song-like-item ${currentTrack._id === track._id || holdOption && selectedTrackId === track._id ? "active-background-like" : ""}`}>
                                    <div className="song-like-info">
                                        <div className="song-like-img">
                                            <Image
                                                src={`${process.env.NEXT_PUBLIC_BACKEND_URL}/img-song/${track.imgUrl}`}
                                                alt={""}
                                                width={60}
                                                height={60}
                                            />
                                            <div className={`icon ${isListTrackPlaying ? "show-equalizer" : ""}`}
                                                onClick={() =>
                                                    handleListTracksPlay ? handleListTracksPlay(track) : null
                                                }
                                            >
                                                {isListTrackPlaying ?
                                                    <Image
                                                        src={"/image/Equalizer-icon-unscreen.gif"}
                                                        alt=""
                                                        width={0}
                                                        height={0}
                                                    />
                                                    : <PlayArrowIcon className="icon-play-track" htmlColor="#fff" fontSize="large" />
                                                }
                                            </div>
                                        </div>

                                        <div className="song-title">
                                            <Link href={`/track/${track._id}?audio=${track.trackUrl}`}><h3 className="song-name">{track.title}</h3></Link>

                                            <div className="song-detail">
                                                <p>{track.singer}</p>
                                            </div>
                                        </div>
                                    </div>

                                    <div className={`track-active-option ${holdOption && selectedTrackId === track._id ? 'show-option' : ""}`}
                                        onClick={() =>
                                            handleHoldTrackClick ? handleHoldTrackClick(track._id) : null
                                        }
                                    >
                                        <LikeTracksDetail
                                            like={like}
                                            paramsId={track._id}
                                        />
                                        <TracksAddPlaylist
                                            setHoldOption={setHoldOption}
                                            holdOption={holdOption}
                                            playlist={playlist}
                                            idTrack={track._id}
                                        />
                                    </div>
                                </div>
                            )
                        })
                    ))}
                </div>
            </div>
        </>
    )
}

export default LikeUserChild