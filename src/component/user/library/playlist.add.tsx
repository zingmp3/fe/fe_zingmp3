'use client'

import { useState } from 'react';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import Modal from '@mui/material/Modal';
import "../../../../public/css/user/library.user.scss";

import AddIcon from '@mui/icons-material/Add';
import { TextField, Tooltip, TooltipProps, styled, tooltipClasses } from '@mui/material';
import { useSession } from 'next-auth/react';
import { useRouter } from 'next/navigation';
import CloseIcon from '@mui/icons-material/Close';
import AddBoxIcon from '@mui/icons-material/AddBox';
import { queryAddPlaylist } from '@/utils/api/user/library.api';


interface IProps {
    holdOption?: boolean | undefined
}

const PlayListAddModal = (props: IProps) => {
    const { holdOption } = props;
    const [open, setOpen] = useState(false);
    const handleOpen = () => setOpen(true);
    const handleClose = () => setOpen(false);
    const { data: session } = useSession()
    const router = useRouter()
    const [titlePlaylist, setTitlePlaylist] = useState('');

    const TooltipIcon = styled(({ className, ...props }: TooltipProps) => (
        <Tooltip {...props} arrow classes={{ popper: className }} />
    ))(({ theme }) => ({
        [`& .${tooltipClasses.arrow}`]: {
            color: "#7D7D7D",
        },
        [`& .${tooltipClasses.tooltip}`]: {
            backgroundColor: "#7D7D7D",
        },
    }));

    const handleAddPlaylist = async () => {
        await queryAddPlaylist(
            titlePlaylist,
            session?.user._id,
            session?.access_token,
            router,
            handleClose,
            setTitlePlaylist,
        )
    }

    return (
        <>
            {holdOption ?
                <div className='create-playlist' onClick={handleOpen}>
                    <span className='create-playlist-icon'><AddBoxIcon fontSize="large" className="icon-active" /></span><span>Tạo playlist mới</span>
                </div>
                :
                <TooltipIcon placement='top' title="Thêm Playlist mới">
                    <AddIcon
                        className='icon-add-playlist'
                        onClick={handleOpen}
                    />
                </TooltipIcon>
            }
            <Modal
                open={open}
                onClose={handleClose}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box className={"modal-add-playlist"}>
                    <Box className="block-close-playlist">
                        <CloseIcon className='close-model-playlist' onClick={handleClose} />
                    </Box>
                    <Typography id="modal-modal-title" color={"#fff"} fontWeight={'600'} variant="h6" component="h2">
                        Tạo playlist mới
                    </Typography>

                    <TextField
                        placeholder='Nhập tên Playlist'
                        className='field-add-playlist'
                        fullWidth
                        value={titlePlaylist}
                        onChange={(e) => setTitlePlaylist(e.target.value)}
                        onKeyDown={(e) => {
                            if (e.code === "Enter") {
                                handleAddPlaylist()
                            }
                        }}
                    />
                    <br />

                    {!titlePlaylist.length ?
                        <Button
                            className={`btn-create-playlist  ${!titlePlaylist.length ? 'drop' : ""}`}
                            fullWidth
                            variant="outlined"
                            disableRipple
                        >
                            Tạo mới
                        </Button>
                        :
                        <Button
                            className={`btn-create-playlist`}
                            fullWidth
                            variant="outlined"
                            disableRipple
                            onClick={() =>
                                handleAddPlaylist()
                            }
                        >
                            Tạo mới
                        </Button>
                    }
                </Box>
            </Modal>
        </>
    );
}

export default PlayListAddModal