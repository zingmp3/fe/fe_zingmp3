'use client'
import { Visibility, VisibilityOff } from '@mui/icons-material';
import LockPersonIcon from '@mui/icons-material/LockPerson';
import { Alert, Box, Button, Divider, IconButton, InputAdornment, Snackbar, TextField } from '@mui/material';
import { useState } from 'react';
import GitHubIcon from '@mui/icons-material/GitHub';
import GoogleIcon from '@mui/icons-material/Google';
import '../../../../public/css/auth/auth.user.scss'
import KeyboardBackspaceIcon from '@mui/icons-material/KeyboardBackspace';
import Link from 'next/link';
import { signIn } from 'next-auth/react';
import { useRouter } from 'next/navigation';




const AuthSub = () => {
    const router = useRouter()
    const [showPassword, setShowPassword] = useState(false);

    const handleClickShowPassword = () => setShowPassword((show) => !show);

    const handleMouseDownPassword = (event: React.MouseEvent<HTMLButtonElement>) => {
        event.preventDefault();
    };

    const [username, setUsername] = useState<string>("");
    const [password, setPassword] = useState<string>("");

    const [erTextUsername, setErTextUsername] = useState<string>("");
    const [erTextPassword, setErTextPassword] = useState<string>("");

    const [erUsername, setErUsername] = useState<boolean>(false);
    const [erPassword, setErPassword] = useState<boolean>(false);

    const [openMessage, setOpenMessage] = useState<boolean>(false)
    const [resMessage, setResMessage] = useState<string>("")


    const getDate = async () => {
        setErUsername(false)
        setErTextUsername("")
        setErTextPassword("")
        setErPassword(false)
        if (!username) {
            setErTextUsername("User is not empty!")
            setErUsername(true)
            return false;
        }

        else if (!password) {
            setErTextPassword("Password is not empty!")
            setErPassword(true)
            return false;
        }

        else if (username && password) {
            const res = await signIn("credentials", {
                username: username,
                password: password,
                redirect: false  //  để không chuyển trang
            })

            console.log("check data", res)


            if (!res?.error) {  // nếu kết quả không có lỗi 
                router.push("/")
            } else {
                setOpenMessage(true)
                setResMessage(res.error)
                console.log("Lỗi đăng nhập")
            }
        }
    }


    return (
        <>
            <div className='auth-main'>
                <div className="logo">
                    <Link href={"/"}><KeyboardBackspaceIcon className='back-page' /></Link>
                    <LockPersonIcon className='logo-auth' />
                </div>

                <p>Sign In</p>

                <Box
                    sx={{
                        '& > :not(style)': { mt: 2 },
                    }}
                >
                    <TextField fullWidth

                        onKeyUp={(e) => {
                            if (e.key === "Enter") {
                                getDate()
                            }
                        }}

                        label="Username"
                        id="fullWidth"
                        onChange={(e) => setUsername(e.target.value)}
                        helperText={erTextUsername}
                        error={erUsername}
                    />
                          <br />

                    <TextField
                        onKeyUp={(e) => {
                            if (e.key === "Enter") {
                                getDate()
                            }
                        }}
                        fullWidth
                        id="outlined-adornment-password"
                        onChange={(e) => setPassword(e.target.value)}
                        helperText={erTextPassword}
                        error={erPassword}
                        label="Password"
                        type={showPassword ? 'text' : 'password'}
                        InputProps={{
                            endAdornment: <InputAdornment position="end" >
                                <IconButton
                                    aria-label="toggle password visibility"
                                    onClick={handleClickShowPassword}
                                    onMouseDown={handleMouseDownPassword}
                                    edge="end"
                                >
                                    {showPassword ? <VisibilityOff /> : <Visibility />}
                                </IconButton>
                            </InputAdornment>
                        }}
                    />
                </Box>


                <Button fullWidth
                    sx={{ marginTop: "20px" }}
                    variant="contained"
                    onClick={() => getDate()}
                >
                    LOGIN
                </Button>

                <Divider sx={{ margin: "20px 0" }}>Or using</Divider>

                <div className="icon-provider">

                    <GitHubIcon onClick={() => signIn("github")} className='icon-provider-item' />

                    <GoogleIcon onClick={() => signIn("google")} className='icon-provider-item' />

                </div>
            </div>

            <Snackbar
                open={openMessage}
                anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
            >
                <Alert
                    onClose={() => setOpenMessage(false)}
                    severity="error"
                    sx={{ width: '100%' }}
                >
                    {resMessage}
                </Alert>
            </Snackbar>
        </>
    )
}


export default AuthSub