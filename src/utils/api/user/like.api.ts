import { sendRequest } from "@/utils/api";

export const queryAddLikeTrack = async (
    like: ILike[],
    idUser: string | undefined,
    token: string | undefined,
    router: any,
    paramsId: string | undefined
) => {
    let id = like.find(data => data.userId._id === idUser)
    const updatedLike = like.map(data => {
        // kiểm tra xem track đã tồn tại trong mảng 
        if (!data.tracks.some(track => track._id === paramsId)) {
            return {
                ...data,
                tracks: [...data.tracks, { _id: paramsId }]
            };
        } else {
            const tracksNew = data.tracks.filter(item => item._id !== paramsId)
            console.log("check tracksNew", tracksNew)
            return {
                ...data,
                tracks: [...tracksNew]
            }
        }
    });

    // tạo một mảng mới chỉ chứa các ID đã được xử lý , thành mảng mới để làm điều kiện cho body
    const updatedTracks = updatedLike.find(data => data.userId._id === idUser)?.tracks.map(track => track._id) || [];
    await sendRequest<IModelPaginate<ILike>>({
        url: `${process.env.NEXT_PUBLIC_BACKEND_URL}/like/${id?._id}`,
        method: "PATCH",
        body: {
            userId: idUser,
            tracks: updatedTracks,
        },
        headers: {
            'Authorization': `Bearer ${token}`,
        },
    })
    await sendRequest<IBackendRes<any>>({  //  clear data cache
        url: `/api/revalidate`,
        method: "POST",
        queryParams: {
            tag: "fetch-like",
        }
    })
    router.refresh();  //  clear router cache 

}
