import { sendRequest } from "@/utils/api"


export const getAllTrack = async (
    token: string | undefined
) => {
    const res = await sendRequest<IModelPaginate<ITracks>>({
        url: `${process.env.NEXT_PUBLIC_BACKEND_URL}/track`,
        method: "GET",
        headers: {
            'Authorization': `Bearer ${token}`,
        },
    })
    return res
}

export const getAllComment = async (
    slug: string,
    token: string | undefined
) => {
    const comment = await sendRequest<IModelPaginate<ITrackComment>>({
        url: `${process.env.NEXT_PUBLIC_BACKEND_URL}/comment`,
        method: "GET",
        queryParams: {
            current: 1,
            pageSize: 100,
            trackId: slug,
            sort: "-createdAt"
        },
        headers: {
            'Authorization': `Bearer ${token}`,
        },
        nextOption: {
            next: { tags: ['fetch-comment'] }
        },
    })
    return comment
}

export const getTracksDetail = async (
    slug: string,
    token: string | undefined
) => {
    const track = await sendRequest<ITracks>({
        url: `${process.env.NEXT_PUBLIC_BACKEND_URL}/track/${slug}`,
        method: "GET",
        headers: {
            'Authorization': `Bearer ${token}`,
        },
    })
    return track
}


export const queryAddTrackPlaylist = async (
    idUser: string | undefined,
    token: string | undefined,
    router: any,
    idTrack: string | undefined,
    playlistItem: any
) => {
    let updatedTracks = playlistItem.tracks.map((track: any) => track._id);

    if (!playlistItem.tracks.some((track: any) => track._id === idTrack)) {
        console.log("check ArrNew:", [...playlistItem.tracks, { _id: idTrack }]);
        updatedTracks = [...updatedTracks, idTrack];
    }
    await sendRequest<IModelPaginate<ILike>>({
        url: `${process.env.NEXT_PUBLIC_BACKEND_URL}/playlist/${playlistItem._id}`,
        method: "PATCH",
        body: {
            userId: idUser,
            tracks: updatedTracks,
        },
        headers: {
            'Authorization': `Bearer ${token}`,
        },
    })
    await sendRequest<IBackendRes<any>>({  //  clear data cache
        url: `/api/revalidate`,
        method: "POST",
        queryParams: {
            tag: "fetch-playlist",
        }
    })
    router.refresh();  //  clear router cache 
}


export const queryPlaylistSearchChange = async (
    query: string,
    setSearchQuery: (v: string) => void,
    token: string | undefined,
    setPlaylistNew: (v: any) => void,
    setPlaylistSuggestions: (v: any) => void,
    initialPlaylists: IPlaylist[]
) => {
    setSearchQuery(query);
    if (token) {
        const res = await sendRequest<IModelPaginate<IPlaylist>>({
            url: `${process.env.NEXT_PUBLIC_BACKEND_URL}/playlist/`,
            method: "GET",
            queryParams: {
                title: `/${query}/i`
            },
            headers: {
                Authorization: `Bearer ${token}`,
            }
        })
        if (res.result) {
            setPlaylistNew(res.result)
            setPlaylistSuggestions(res.result)
        }
    } else {
        setPlaylistNew(initialPlaylists);
    }
}





