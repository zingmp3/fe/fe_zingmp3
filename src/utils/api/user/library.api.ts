import { sendRequest } from "@/utils/api"


export const getAllPlaylist = async (
    token: string | undefined,
    idUser: string | undefined
) => {
    const playlist = await sendRequest<IModelPaginate<IPlaylist>>({
        url: `${process.env.NEXT_PUBLIC_BACKEND_URL}/playlist`,
        method: "GET",
        queryParams: {
            current: 1,
            userId: idUser,
            sort: "-createdAt"
        },
        headers: {
            'Authorization': `Bearer ${token}`,
        },
        nextOption: {
            next: { tags: ['fetch-playlist'] }
        },
    })
    return playlist
}

export const getAllLike = async (
    idUser: string | undefined,
    token: string | undefined,
) => {
    const like = await sendRequest<IModelPaginate<ILike>>({
        url: `${process.env.NEXT_PUBLIC_BACKEND_URL}/like`,
        method: "GET",
        queryParams: {
            current: 1,
            userId: idUser,
            sort: "-createdAt"
        },
        headers: {
            'Authorization': `Bearer ${token}`,
        },
        nextOption: {
            // cache: "no-store" 
            next: { tags: ['fetch-like'] }
        },
    })
    return like;
}


export const queryAddPlaylist = async (
    titlePlaylist: string,
    idUser: string | undefined,
    token: string | undefined,
    router: any,
    handleClose: () => void,
    setTitlePlaylist: (v: string) => void
) => {
    const playlist = await sendRequest<IModelPaginate<IPlaylist>>({
        url: `${process.env.NEXT_PUBLIC_BACKEND_URL}/playlist`,
        method: "POST",
        body: {
            title: titlePlaylist,
            userId: idUser
        },
        headers: {
            'Authorization': `Bearer ${token}`,
        },
    })
    if (playlist) {
        await sendRequest<IBackendRes<any>>({  //  clear data cache
            url: `/api/revalidate`,
            method: "POST",
            queryParams: {
                tag: "fetch-playlist",
            }
        })
        router.refresh();  //  clear router cache 
        handleClose()
        setTitlePlaylist("")
    }

}

