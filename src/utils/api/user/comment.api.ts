import { sendRequest } from "@/utils/api";

export const queryCreateComment = async (
    contentComment: string,
    currentTrack: IShareTrack,
    idUser: string | undefined,
    token: string | undefined,
    setContentComment: (v: string) => void,
    router: any,
) => {
    const res = await sendRequest<IModelPaginate<ITrackComment>>({
        url: `${process.env.NEXT_PUBLIC_BACKEND_URL}/comment`,
        method: "POST",
        body: {
            content: contentComment,
            trackId: currentTrack._id,
            userId: idUser,
        },
        headers: {
            'Authorization': `Bearer ${token}`,
        },
    })
    if (res) {
        setContentComment("")
    }
    await sendRequest<IBackendRes<any>>({  //  clear data cache
        url: `/api/revalidate`,
        method: "POST",
        queryParams: {
            tag: "fetch-comment",
        }
    })

    router.refresh();  //  clear router cache 
}



