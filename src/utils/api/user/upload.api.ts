import { sendRequest, sendRequestFile } from "@/utils/api";


export const queryDeleteTrackUpload = async (
    filename: string | undefined,
    token: string | undefined,
    router: any,
) => {
    await sendRequest<any>({
        url: `${process.env.NEXT_PUBLIC_BACKEND_URL}/file/delete/${filename}`,
        method: "DELETE",
        headers: {
            'Authorization': `Bearer ${token}`,
        },
    });
    await sendRequest<IBackendRes<any>>({  //  clear data cache
        url: `/api/revalidate`,
        method: "POST",
        queryParams: {
            tag: "fetch-upload",
        }
    })
    router.refresh();  //  clear router cache 
}

export const queryUploadFileTrack = async (
    formData: any,
    token: string | undefined,
    router: any,
) => {
    await sendRequestFile<any>({
        url: `${process.env.NEXT_PUBLIC_BACKEND_URL}/file/upload/`,
        method: "POST",
        body: formData,
        headers: {
            'Authorization': `Bearer ${token}`,
            'yop': 'upload',
        },
    });
    await sendRequest<IBackendRes<any>>({  //  clear data cache
        url: `/api/revalidate`,
        method: "POST",
        queryParams: {
            tag: "fetch-upload",
        }
    })
    router.refresh();  //  clear router cache 
}


export const getAllUpload = async (
    token: string | undefined
) => {
    const uploads = await sendRequest<any>({
        url: `${process.env.NEXT_PUBLIC_BACKEND_URL}/file/tracks`,
        method: "GET",
        headers: {
            'Authorization': `Bearer ${token}`,
        },

        nextOption: {
            next: { tags: ['fetch-upload'] }
        },
    })

    return uploads;

}