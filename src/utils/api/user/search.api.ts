import { sendRequest } from "@/utils/api"


export const querySearchTracks = async (
    params: string | null,
    token: any,
) => {
    const res = await sendRequest<IBackendRes<ITracks[]>>({
        url: `${process.env.NEXT_PUBLIC_BACKEND_URL}/track`,
        method: "GET",
        queryParams: {
            current: 1,
            title: `/${params}/i`
        },
        headers: {
            'Authorization': `Bearer ${token}`,
        },
        nextOption: {
            next: { tags: ['fetch-search'] }
        },
    })
    return res?.result

}