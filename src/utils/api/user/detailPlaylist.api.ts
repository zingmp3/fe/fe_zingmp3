import { sendRequest } from "@/utils/api"

export const getDetailPlaylist = async (
    slug: string,
    token: string | undefined
) => {
    const playlist = await sendRequest<IPlaylist>({
        url: `${process.env.NEXT_PUBLIC_BACKEND_URL}/playlist/${slug}`,
        method: "GET",
        headers: {
            'Authorization': `Bearer ${token}`,
        },
        nextOption: {
            next: { tags: ['fetch-detail-playlist'] }
        },
    })
    return playlist
}


export const queryEditPlaylist = async (
    id: string,
    titlePlaylist: string,
    idUser: string | undefined,
    token: string | undefined,
    router: any,
    handleClose: () => void,
    handleCloseMenu: (v: boolean) => void,
    setTitlePlaylist: (v: string) => void,
) => {
    const playlistEdit = await sendRequest<IModelPaginate<IPlaylist>>({
        url: `${process.env.NEXT_PUBLIC_BACKEND_URL}/playlist/${id}`,
        method: "PATCH",
        body: {
            title: titlePlaylist,
            userId: idUser
        },
        headers: {
            'Authorization': `Bearer ${token}`,
        },
    })
    if (playlistEdit) {
        await sendRequest<IBackendRes<any>>({  //  clear data cache
            url: `/api/revalidate`,
            method: "POST",
            queryParams: {
                tag: "fetch-playlist",
            }
        })
        router.refresh();  //  clear router cache 

        handleClose()
        if (handleCloseMenu) {
            handleCloseMenu(false)
        }
        setTitlePlaylist("")
    }
}


export const queryDeletePlaylist = async (
    id: string,
    token: string | undefined,
    router: any,
    handleCloseDelete: () => void,
) => {
    await sendRequest<IModelPaginate<IPlaylist>>({
        url: `${process.env.NEXT_PUBLIC_BACKEND_URL}/playlist/${id}`,
        method: "DELETE",
        headers: {
            'Authorization': `Bearer ${token}`,
        },
    })
    await sendRequest<IBackendRes<any>>({  //  clear data cache
        url: `/api/revalidate`,
        method: "POST",
        queryParams: {
            tag: "fetch-playlist",
        }
    })
    router.refresh();  //  clear router cache 
    handleCloseDelete();
}


export const queryDeleteTrackOnPlaylist = async (
    playlist: IPlaylist,
    paramsId: string | undefined,
    userId: string | undefined,
    token: string | undefined,
    router: any,
) => {
    const updatedTracks = playlist.tracks.filter(data => data._id !== paramsId) || [];
    await sendRequest<IModelPaginate<IPlaylist>>({
        url: `${process.env.NEXT_PUBLIC_BACKEND_URL}/playlist/${playlist._id}`,
        method: "PATCH",
        body: {
            userId: userId,
            tracks: updatedTracks,
        },
        headers: {
            'Authorization': `Bearer ${token}`,
        },
    })
    await sendRequest<IBackendRes<any>>({  //  clear data cache
        url: `/api/revalidate`,
        method: "POST",
        queryParams: {
            tag: "fetch-detail-playlist",
        }
    })
    router.refresh();  //  clear router cache 

}





