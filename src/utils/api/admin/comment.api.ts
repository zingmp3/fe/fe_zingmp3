
import { sendRequest } from "@/utils/api"



export const commentListPagination = async (token: string, idUser: string | undefined) => {
    const res = await sendRequest<IModelPaginate<ITrackComment>>({
        url: `${process.env.NEXT_PUBLIC_BACKEND_URL}/comment`,
        method: "GET",
        queryParams: {
            current: 1,
            pageSize: 10,
            userId: idUser,
            sort: "-createdAt"
        },

        headers: {
            'Authorization': `Bearer ${token}`,
        },

    })
    return res;
}

//  Phần Search
export const handleSearchComment = async (
    query: string,
    setSearchQuery: (v: string) => void,
    page: number,
    rowsPerPage: number,
    token: string | undefined,
    setComment: (v: any) => void,
    setMetaData: (v: {
        current: number,
        pageSize: number,
        pages: number,
        total: number
    }) => void,
    setCommentSuggestions: (v: any) => void,
    meta: {
        current: number,
        pageSize: number,
        pages: number,
        total: number,
    },
    initialComments: ITrackComment[]
) => {
    setSearchQuery(query);
    if (query.length >= 1) {
        const res = await sendRequest<IModelPaginate<ITrackComment>>({
            url: `${process.env.NEXT_PUBLIC_BACKEND_URL}/comment/`,
            method: "GET",
            queryParams: {
                current: page + 1,
                pageSize: rowsPerPage,
                content: `/${query}/i`
            },
            headers: {
                Authorization: `Bearer ${token}`,
            }
        });

        if (res.result && res.meta) {
            setComment(res.result)
            setMetaData({
                current: res.meta.current,
                pageSize: res.meta.pageSize,
                pages: res.meta.pages,
                total: res.meta.total
            })
            setCommentSuggestions(res.result);
        }
    }
    else {
        setComment(initialComments);
        setMetaData(meta)
    }
}

// Phần chuyển tiếp trang
export const handleChangeDataTableNextPageComment = async (
    page: number,
    rowsPerPage: number,
    token: string | undefined,
    setComment: (v: any) => void,
    setMetaData: (v: {
        current: number,
        pageSize: number,
        pages: number,
        total: number
    }) => void,
) => {

    if (token) {
        const res = await sendRequest<IModelPaginate<ITrackComment>>({
            url: `${process.env.NEXT_PUBLIC_BACKEND_URL}/comment/`,
            method: "GET",
            queryParams: {
                // lùi trang (giảm page), chỉ số page sẽ giảm, nhưng khi gửi yêu cầu đến server,cần gửi chỉ số trang theo định dạng one-based index (Chỉ số dựa trên một)
                current: page + 1,
                pageSize: rowsPerPage
            },
            headers: {
                Authorization: `Bearer ${token}`,
            }
        })
        if (res.result && res.meta) {
            setComment(res.result)
            setMetaData({
                current: res.meta.current,
                pageSize: res.meta.pageSize,
                pages: res.meta.pages,
                total: res.meta.total
            })
        }
    }
}


export const handleDeleteComment = async (
    id: string,
    page: number,
    token: string | undefined,
    setComment: (v: any) => void,
    setMetaData: (v: {
        current: number,
        pageSize: number,
        pages: number,
        total: number
    }) => void,
    setOpenSnack: (v: boolean) => void,
    setOpenDelete: (v: boolean) => void

) => {
    if (token) {
        const res = await sendRequest<IModelPaginate<ITrackComment>>({
            url: `${process.env.NEXT_PUBLIC_BACKEND_URL}/comment/${id}`,
            method: "DELETE",
            headers: {
                Authorization: `Bearer ${token}`,
            }
        })
        if (res) {
            const getData = await sendRequest<IModelPaginate<ITrackComment>>({
                url: `${process.env.NEXT_PUBLIC_BACKEND_URL}/comment`,
                method: "GET",
                queryParams: {
                    current: page + 1,
                    pageSize: 10,
                    sort: "-createdAt"
                },
                headers: {
                    'Authorization': `Bearer ${token}`,
                },

            })
            setComment(getData.result)
            setMetaData(getData.meta)

            setOpenSnack(true)
            setOpenDelete(false)
        }
    }
}




