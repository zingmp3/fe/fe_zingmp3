import { sendRequest } from "@/utils/api"


export const userListPagination = async (token: string) => {
  const res = await sendRequest<IModelPaginate<IUser>>({
    url: `${process.env.NEXT_PUBLIC_BACKEND_URL}/user`,
    method: "GET",
    queryParams: {
      current: 1,
      pageSize: 10,
      sort: "-createdAt"
    },
    headers: {
      'Authorization': `Bearer ${token}`,
    },
  })
  return res
}


export const handleSearchUser = async (
  query: string,
  setSearchQuery: (v: string) => void,
  page: number,
  rowsPerPage: number,
  token: string | undefined,
  setUser: (v: any) => void,
  setMetaData: (v: {
    current: number,
    pageSize: number,
    pages: number,
    total: number
  }) => void,
  setUserSuggestions: (v: any) => void,
  meta: {
    current: number,
    pageSize: number,
    pages: number,
    total: number,
  },
  initialUser: IUser[]
) => {
  setSearchQuery(query);

  if (query.length >= 1) {
    const res = await sendRequest<IModelPaginate<IUser>>({
      url: `${process.env.NEXT_PUBLIC_BACKEND_URL}/user/`,
      method: "GET",
      queryParams: {
        current: page + 1,
        pageSize: rowsPerPage,
        email: `/${query}/i`
      },
      headers: {
        Authorization: `Bearer ${token}`,
      }
    });
    if (res.result && res.meta) {
      setUser(res.result)
      setMetaData({
        current: res.meta.current,
        pageSize: res.meta.pageSize,
        pages: res.meta.pages,
        total: res.meta.total
      })
      setUserSuggestions(res.result);
    }
  }
  else {
    setUser(initialUser);
    setMetaData(meta)
  }
}


export const handleChangeDataTableNextPageUser = async (
  page: number,
  rowsPerPage: number,
  token: string | undefined,
  setUser: (v: any) => void,
  setMetaData: (v: {
    current: number,
    pageSize: number,
    pages: number,
    total: number,
  }) => void,
) => {
  if (token) {
    const res = await sendRequest<IModelPaginate<IUser>>({
      url: `${process.env.NEXT_PUBLIC_BACKEND_URL}/user/`,
      method: "GET",
      queryParams: {
        // lùi trang (giảm page), chỉ số page sẽ giảm, nhưng khi gửi yêu cầu đến server,cần gửi chỉ số trang theo định dạng one-based index (Chỉ số dựa trên một)
        current: page + 1,
        pageSize: rowsPerPage
      },
      headers: {
        Authorization: `Bearer ${token}`,
      }
    })
    if (res.result && res.meta) {
      setUser(res.result)
      setMetaData({
        current: res.meta.current,
        pageSize: res.meta.pageSize,
        pages: res.meta.pages,
        total: res.meta.total
      })
    }
  }
}

// Thêm user
export const queryCreateUser = async (
  token: string | undefined,
  email: string,
  password: string,
  name: string,
  age: string,
  address: string,
  type: string,
  role: string,
  setUser: (v: any) => void,
  setMetaData: (v: any) => void,
  setOpen: (v: boolean) => void,
  setOpenSnackAdd: (v: boolean) => void,
  setEmail: (v: any) => void,
  setPassword: (v: any) => void,
  setName: (v: any) => void,
  setAge: (v: any) => void,
  setAddress: (v: any) => void,
  setType: (v: any) => void,
  setRole: (v: any) => void,
) => {
  const res = await sendRequest<IModelPaginate<IUser>>({
    url: `${process.env.NEXT_PUBLIC_BACKEND_URL}/user/`,
    method: "POST",
    body: {
      email: email,
      password: password,
      name: name,
      age: age,
      address: address,
      type: type,
      role: role,
    },
    headers: {
      Authorization: `Bearer ${token}`,
    }
  })
  if (res) {
    const getData = await sendRequest<IModelPaginate<IUser>>({
      url: `${process.env.NEXT_PUBLIC_BACKEND_URL}/user`,
      method: "GET",
      queryParams: {
        current: 1,
        pageSize: 10,
        sort: "-createdAt"
      },
      headers: {
        'Authorization': `Bearer ${token}`,
      },
    })

    setUser(getData.result)
    setMetaData(getData.meta)
    setOpen(false);
    setOpenSnackAdd(true)

    // khi create xong thì làm trống field
    setEmail("")
    setPassword("")
    setName("")
    setAge("")
    setAddress("")
    setType("")
    setRole("")
  }
}

export const queryEditUser = async (
  id: string,
  token: string | undefined,
  page: number,
  currentData: {
    email: string,
    name: string,
    age: number,
    address: string,
    type: string,
    role: string
  },
  setUser: (v: any) => void,
  setMetaData: (v: any) => void,
  setOpenEdit: (v: any) => void,
  setOpenSnackEdit: (v: any) => void,
) => {
  const res = await sendRequest<IModelPaginate<IUser>>({
    url: `${process.env.NEXT_PUBLIC_BACKEND_URL}/user/${id}`,
    method: "PATCH",
    body: {
      email: currentData.email,
      name: currentData.name,
      age: currentData.age,
      address: currentData.address,
      type: currentData.type,
      role: currentData.role,
    },
    headers: {
      Authorization: `Bearer ${token}`,
    }
  })
  if (res) {
    const getData = await sendRequest<IModelPaginate<IUser>>({
      url: `${process.env.NEXT_PUBLIC_BACKEND_URL}/user`,
      method: "GET",
      queryParams: {
        current: page + 1,
        pageSize: 10,
        sort: "-createdAt"
      },
      headers: {
        'Authorization': `Bearer ${token}`,
      },
    })
    setUser(getData.result)
    setMetaData(getData.meta)
    setOpenEdit(false);
    setOpenSnackEdit(true)
  }
}


export const queryDeleteUser = async (
  id: string,
  token: string | undefined,
  page: number,
  setUser: (v: any) => void,
  setMetaData: (v: any) => void,
  setOpenSnackDelete: (v: boolean) => void,
  setOpenDelete: (v: boolean) => void,
) => {
  if (token) {
    const res = await sendRequest<IModelPaginate<IUser>>({
      url: `${process.env.NEXT_PUBLIC_BACKEND_URL}/user/${id}`,
      method: "DELETE",
      headers: {
        Authorization: `Bearer ${token}`,
      }
    })
    if (res) {
      const getData = await sendRequest<IModelPaginate<IUser>>({
        url: `${process.env.NEXT_PUBLIC_BACKEND_URL}/user`,
        method: "GET",
        queryParams: {
          current: page + 1,
          pageSize: 10,
          sort: "-createdAt"
        },
        headers: {
          'Authorization': `Bearer ${token}`,
        },
      })
      setUser(getData.result)
      setMetaData(getData.meta)
      setOpenSnackDelete(true)
      setOpenDelete(false)
    }
  }
}
