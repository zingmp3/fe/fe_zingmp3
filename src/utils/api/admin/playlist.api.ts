import { sendRequest } from "@/utils/api"


export const playlistListPagination = async (token: string) => {
  const res = await sendRequest<IModelPaginate<IPlaylist>>({
    url: `${process.env.NEXT_PUBLIC_BACKEND_URL}/playlist`,
    method: "GET",
    queryParams: {
      current: 1,
      pageSize: 10,
      sort: "-createdAt"
    },
    headers: {
      'Authorization': `Bearer ${token}`,
    },
  })
  return res
}


export const handleSearchChangePlaylist = async (
  query: string,
  setSearchQuery: (v: string) => void,
  page: number,
  rowsPerPage: number,
  token: string | undefined,
  setPlaylist: (v: any) => void,
  setMetaData: (v: {
    current: number,
    pageSize: number,
    pages: number,
    total: number
  }) => void,
  setPlaylistSuggestions: (v: any) => void,
  meta: {
    current: number,
    pageSize: number,
    pages: number,
    total: number,
  },
  initialPlaylists: IPlaylist[]
) => {
  setSearchQuery(query);

  if (query.length >= 1) {
    const res = await sendRequest<IModelPaginate<IPlaylist>>({
      url: `${process.env.NEXT_PUBLIC_BACKEND_URL}/playlist/`,
      method: "GET",
      queryParams: {
        current: page + 1,
        pageSize: rowsPerPage,
        title: `/${query}/i`
      },
      headers: {
        Authorization: `Bearer ${token}`,
      }
    });

    if (res.result && res.meta) {
      setPlaylist(res.result)
      setMetaData({
        current: res.meta.current,
        pageSize: res.meta.pageSize,
        pages: res.meta.pages,
        total: res.meta.total
      })
      setPlaylistSuggestions(res.result);
    }
  }
  else {
    setPlaylist(initialPlaylists);
    setMetaData(meta)
  }
}


// Phần chuyển tiếp trang
export const handleChangeDataTableNextPagePlaylist = async (
  page: number,
  rowsPerPage: number,
  token: string | undefined,
  setPlaylist: (v: any) => void,
  setMetaData: (v: {
    current: number,
    pageSize: number,
    pages: number,
    total: number
  }) => void,
) => {

  if (token) {
    const res = await sendRequest<IModelPaginate<IPlaylist>>({
      url: `${process.env.NEXT_PUBLIC_BACKEND_URL}/playlist/`,
      method: "GET",
      queryParams: {
        // lùi trang (giảm page), chỉ số page sẽ giảm, nhưng khi gửi yêu cầu đến server,cần gửi chỉ số trang theo định dạng one-based index (Chỉ số dựa trên một)
        current: page + 1,
        pageSize: rowsPerPage
      },
      headers: {
        Authorization: `Bearer ${token}`,
      }
    })

    if (res.result && res.meta) {
      setPlaylist(res.result)
      setMetaData({
        current: res.meta.current,
        pageSize: res.meta.pageSize,
        pages: res.meta.pages,
        total: res.meta.total
      })
    }
  }
}


export const handleDeletePlaylist = async (
  id: string,
  page: number,
  token: string | undefined,
  setPlaylist: (v: any) => void,
  setMetaData: (v: {
    current: number,
    pageSize: number,
    pages: number,
    total: number
  }) => void,
  setOpenSnack: (v: boolean) => void,
  setOpenDelete: (v: boolean) => void
) => {
  if (token) {
    const res = await sendRequest<IModelPaginate<IPlaylist>>({
      url: `${process.env.NEXT_PUBLIC_BACKEND_URL}/playlist/${id}`,
      method: "DELETE",
      headers: {
        Authorization: `Bearer ${token}`,
      }
    })

    if (res) {
      const getData = await sendRequest<IModelPaginate<IPlaylist>>({
        url: `${process.env.NEXT_PUBLIC_BACKEND_URL}/playlist`,
        method: "GET",
        queryParams: {
          current: page + 1,
          pageSize: 10,
          sort: "-createdAt"
        },
        headers: {
          'Authorization': `Bearer ${token}`,
        },
      })
      setPlaylist(getData.result)
      setMetaData(getData.meta)

      setOpenSnack(true)
      setOpenDelete(false)
    }
  }

}
