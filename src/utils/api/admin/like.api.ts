import { sendRequest } from "@/utils/api"

export const likeListPagination = async (token: string,idUser: string | undefined) => {
  const res = await sendRequest<IModelPaginate<ILike>>({
    url: `${process.env.NEXT_PUBLIC_BACKEND_URL}/like`,
    method: "GET",
    queryParams: {
      current: 1,
      pageSize: 10,
      userId: idUser,
      sort: "-createdAt"
    },
    headers: {
      'Authorization': `Bearer ${token}`,
    },
    nextOption: {
      // cache: "no-store" 
      next: { tags: ['fetch-like'] }
    },
  })
  return res
}


export const handleSearchLike = async (
  query: string,
  setSearchQuery: (v: string) => void,
  page: number,
  rowsPerPage: number,
  token: string | undefined,
  setLike: (v: any) => void,
  setMetaData: (v: {
    current: number,
    pageSize: number,
    pages: number,
    total: number
  }) => void,
  setlikeSuggestions: (v: any) => void,
  meta: {
    current: number,
    pageSize: number,
    pages: number,
    total: number,
  },
  initialLikes: ILike[]
) => {
  setSearchQuery(query);

  if (query.length >= 1) {
    const res = await sendRequest<IModelPaginate<ILike>>({
      url: `${process.env.NEXT_PUBLIC_BACKEND_URL}/like/`,
      method: "GET",
      queryParams: {
        current: page + 1,
        pageSize: rowsPerPage,
        'userId.email': `/${query}/i` // bị false
      },
      headers: {
        Authorization: `Bearer ${token}`,
      }
    });

    if (res.result && res.meta) {
      setLike(res.result)
      setMetaData({
        current: res.meta.current,
        pageSize: res.meta.pageSize,
        pages: res.meta.pages,
        total: res.meta.total
      })
      setlikeSuggestions(res.result);
    }
  }
  else {
    setLike(initialLikes);
    setMetaData(meta)
  }
}


// Phần chuyển tiếp trang
export const handleChangeDataTableNextPageLike = async (
  page: number,
  rowsPerPage: number,
  token: string | undefined,
  setComment: (v: any) => void,
  setMetaData: (v: {
    current: number,
    pageSize: number,
    pages: number,
    total: number
  }) => void,
) => {

  if (token) {
    const res = await sendRequest<IModelPaginate<ILike>>({
      url: `${process.env.NEXT_PUBLIC_BACKEND_URL}/like/`,
      method: "GET",
      queryParams: {
        // lùi trang (giảm page), chỉ số page sẽ giảm, nhưng khi gửi yêu cầu đến server,cần gửi chỉ số trang theo định dạng one-based index (Chỉ số dựa trên một)
        current: page + 1,
        pageSize: rowsPerPage
      },
      headers: {
        Authorization: `Bearer ${token}`,
      }
    })

    if (res.result && res.meta) {
      setComment(res.result)
      setMetaData({
        current: res.meta.current,
        pageSize: res.meta.pageSize,
        pages: res.meta.pages,
        total: res.meta.total
      })
    }
  }
}


export const handleDeleteLike = async (
  id: string,
  page: number,
  token: string | undefined,
  setComment: (v: any) => void,
  setMetaData: (v: {
    current: number,
    pageSize: number,
    pages: number,
    total: number
  }) => void,
  setOpenSnack: (v: boolean) => void,
  setOpenDelete: (v: boolean) => void
) => {
  if (token) {
    const res = await sendRequest<IModelPaginate<ILike>>({
      url: `${process.env.NEXT_PUBLIC_BACKEND_URL}/like/${id}`,
      method: "DELETE",
      headers: {
        Authorization: `Bearer ${token}`,
      }
    })

    if (res) {
      const getData = await sendRequest<IModelPaginate<ILike>>({
        url: `${process.env.NEXT_PUBLIC_BACKEND_URL}/like`,
        method: "GET",
        queryParams: {
          current: page + 1,
          pageSize: 10,
          sort: "-createdAt"
        },
        headers: {
          'Authorization': `Bearer ${token}`,
        },
      })
      setComment(getData.result)
      setMetaData(getData.meta)

      setOpenSnack(true)
      setOpenDelete(false)
    }
  }

}









