import { sendRequest } from "@/utils/api"

export const trackListPagination = async (token: string) => {
  const res = await sendRequest<IModelPaginate<ITracks>>({
    url: `${process.env.NEXT_PUBLIC_BACKEND_URL}/track`,
    method: "GET",
    queryParams: {
      current: 1,
      pageSize: 10,
      sort: "-createdAt"
    },
    headers: {
      'Authorization': `Bearer ${token}`,
    },
  })
  return res
}


export const handleSearchChangeTracks = async (
  query: string,
  setSearchQuery: (v: string) => void,
  page: number,
  rowsPerPage: number,
  token: string | undefined,
  setTrack: (v: any) => void,
  setMetaData: (v: {
    current: number,
    pageSize: number,
    pages: number,
    total: number
  }) => void,
  setTrackSuggestions: (v: any) => void,
  meta: {
    current: number,
    pageSize: number,
    pages: number,
    total: number,
  },
  initialTracks: ITracks[]

) => {
  setSearchQuery(query);

  if (query.length >= 1) {
    const res = await sendRequest<IModelPaginate<ITracks>>({
      url: `${process.env.NEXT_PUBLIC_BACKEND_URL}/track/`,
      method: "GET",
      queryParams: {
        current: page + 1,
        pageSize: rowsPerPage,
        title: `/${query}/i`
      },
      headers: {
        Authorization: `Bearer ${token}`,
      }
    });

    if (res.result && res.meta) {
      setTrack(res.result)
      setMetaData({
        current: res.meta.current,
        pageSize: res.meta.pageSize,
        pages: res.meta.pages,
        total: res.meta.total
      })
      setTrackSuggestions(res.result);
    }
  }
  else {
    setTrack(initialTracks); // Reset to initial data when search query is empty
    setMetaData(meta)
  }
}


// Phần chuyển tiếp trang
export const handleChangeDataTableNextPageTracks = async (
  page: number,
  rowsPerPage: number,
  token: string | undefined,
  setTrack: (v: any) => void,
  setMetaData: (v: {
    current: number,
    pageSize: number,
    pages: number,
    total: number
  }) => void,
) => {

  if (token) {
    const res = await sendRequest<IModelPaginate<ITracks>>({
      url: `${process.env.NEXT_PUBLIC_BACKEND_URL}/track/`,
      method: "GET",
      queryParams: {
        // lùi trang (giảm page), chỉ số page sẽ giảm, nhưng khi gửi yêu cầu đến server,cần gửi chỉ số trang theo định dạng one-based index (Chỉ số dựa trên một)
        current: page + 1,
        pageSize: rowsPerPage
      },
      headers: {
        Authorization: `Bearer ${token}`,
      }
    })
    if (res.result && res.meta) {
      setTrack(res.result)
      setMetaData({
        current: res.meta.current,
        pageSize: res.meta.pageSize,
        pages: res.meta.pages,
        total: res.meta.total
      })
    }
  }
}


export const handleDeleteTrack = async (
  id: string,
  page: number,
  token: string | undefined,
  setTrack: (v: any) => void,
  setMetaData: (v: {
    current: number,
    pageSize: number,
    pages: number,
    total: number
  }) => void,
  setOpenSnack: (v: boolean) => void,
  setOpenDelete: (v: boolean) => void

) => {
  if (token) {
    const res = await sendRequest<IModelPaginate<ITracks>>({
      url: `${process.env.NEXT_PUBLIC_BACKEND_URL}/track/${id}`,
      method: "DELETE",
      headers: {
        Authorization: `Bearer ${token}`,
      }
    })
    if (res) {
      const getData = await sendRequest<IModelPaginate<ITracks>>({
        url: `${process.env.NEXT_PUBLIC_BACKEND_URL}/track`,
        method: "GET",
        queryParams: {
          current: page + 1,
          pageSize: 10,
          sort: "-createdAt"
        },
        headers: {
          'Authorization': `Bearer ${token}`,
        },
      })
      setTrack(getData.result)
      setMetaData(getData.meta)

      setOpenSnack(true)
      setOpenDelete(false)
    }
  }
}
