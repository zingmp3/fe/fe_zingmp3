
import { JWT } from "next-auth/jwt";
import GithubProvider from "next-auth/providers/github"
import CredentialsProvider from "next-auth/providers/credentials";
import GoogleProvider from "next-auth/providers/google"
import{ AuthOptions } from "next-auth"

import dayjs from "dayjs";
import NextAuth from "next-auth/next";
import { sendRequest } from "@/utils/api";




export const authOptions: AuthOptions = {
    secret: process.env.NEXTAUTH_SECRET,
    // Configure one or more authentication providers
    providers: [

        CredentialsProvider({
            // The name to display on the sign in form (e.g. "Sign in with...")
            name: "Credentials",
            // `credentials` is used to generate a form on the sign in page.
            // You can specify which fields should be submitted, by adding keys to the `credentials` object.
            // e.g. domain, username, password, 2FA token, etc.
            // You can pass any HTML attribute to the <input> tag through the object.
            credentials: {
              username: { label: "Username", type: "text"},
              password: { label: "Password", type: "password" }
            },
            async authorize(credentials, req) {
              // Add logic here to look up the user from the credentials supplied
              const res = await sendRequest<JWT>({
                url: `${process.env.NEXT_PUBLIC_BACKEND_URL}/auth/login`,
                method: "POST",
                body: {
                    username: credentials?.username,
                    password: credentials?.password
                }
            })

              if (res.user) {
                // Any object returned will be saved in `res` property of the JWT
                return res as any
              } else {
                // If you return null then an error will be displayed advising the user to check their details.
                throw new Error(res.message as string)

                // You can also Reject this callback with an Error thus the user will be sent to the error page with the error message as a query parameter
              }
            }
          }),


        GithubProvider({
            clientId: process.env.GITHUB_ID!,
            clientSecret: process.env.GITHUB_SECRET!,
        }),
        // ...add more providers here
        GoogleProvider({
            clientId: process.env.GOOGLE_ID!,
            clientSecret: process.env.GOOGLE_SECRET!,
        }),
    ],



    callbacks: {
        async jwt({ token, user, account, profile, trigger }) {
          
          if (trigger === "signIn" && account?.provider !== "credentials") {
            const res = await sendRequest<JWT>({
              url: `${process.env.NEXT_PUBLIC_BACKEND_URL}/auth/social-media`,
              method: "POST",
              body: {
                type: account?.provider.toLocaleUpperCase(),
                username: user.email
              }
            });

            if (res) {
              token.access_token = res.access_token;
              token.refresh_token = res.refresh_token;
              token.user = res.user;
              token.access_expire = dayjs(new Date()).add(
                +(process.env.TOKEN_EXPIRE_NUMBER as string), (process.env.TOKEN_EXPIRE_UNIT as any)
              ).unix();
            }
          }

          if (trigger === "signIn" && account?.provider === "credentials") {
            //@ts-ignore
            token.access_token = user.access_token;
            //@ts-ignore
            token.refresh_token = user.refresh_token;
            //@ts-ignore
            token.user = user.user;
            token.access_expire = dayjs(new Date()).add(
              +(process.env.TOKEN_EXPIRE_NUMBER as string), (process.env.TOKEN_EXPIRE_UNIT as any)
            ).unix();
          }

        //   const isTimeAfter = dayjs(new Date()).isAfter(dayjs.unix(token?.access_expire ?? 0));
        //   console.log("isTimeAfter:", isTimeAfter, "token.access_expire:", token.access_expire);

        //   if (isTimeAfter) {
        //     token = await refreshAccessToken(token);
        //   }

          return token;
        },




        session({ session, token, user }) {
            // console.log("Session gọi lại với token:", token);
            if (token) {
              session.access_token = token.access_token;
              session.refresh_token = token.refresh_token;
              session.user = token.user;
              session.access_expire = token.access_expire;
              session.error = token.error;
            }
            return session
          }

        }

}




// import NextAuth from "next-auth"
// import GithubProvider from "next-auth/providers/github"
// import { AuthOptions } from "next-auth"

// import { JWT } from "next-auth/jwt"
// import CredentialsProvider from "next-auth/providers/credentials"
// import { sendRequest } from "../../../../utils/api"

// export const authOptions: AuthOptions = {
//   secret: process.env.NO_SECRET,

//   // Configure one or more authentication providers
//   providers: [

//     GithubProvider({
//       clientId: process.env.GITHUB_ID!,
//       clientSecret: process.env.GITHUB_SECRET!,
//     }),

//     CredentialsProvider({
//       // The name to display on the sign in form (e.g. 'Sign in with...')
//       name: 'Credentials',
//       // The credentials is used to generate a suitable form on the sign in page.
//       // You can specify whatever fields you are expecting to be submitted.
//       // e.g. domain, username, password, 2FA token, etc.
//       // You can pass any HTML attribute to the <input> tag through the object.
//       credentials: {
//         username: { label: "Username", type: "text" },
//         password: { label: "Password", type: "password" }
//       },
//       async authorize(credentials, req) {

//         const res = await sendRequest<JWT>({
//           url: "http://localhost:8000/auth/login/",
//           method: "POST",
//           queryParams: {
//             username: credentials?.username,
//             password: credentials?.password
//           }
//         })


//         // If no error and we have user data, return it
//         if (res.user && res) {
//           return res as any
//         } else {
//           // Return null if user data could not be retrieved
//           throw new Error(res.message as string)
//         }

//       }
//     })
//   ],

//   //  Làm cái phần Auth này nhiều khi code xong phải npm run dev lại code thì nó mới ăn 
//   // (Hoặc TK bên BE lỗi phải xóa đi tạo cái khác)

//   callbacks: {  // callbacks sẽ được gọi bất cứ khi nào JWT được tạo ra (nghĩa là khi signIn, update sesion) , và nội dung của JWT sẽ được chuyển tiếp đến phần sesion callback 
//     async jwt({ token, user, account, profile, trigger }) {
//       if (trigger === "signIn" && account?.provider !== "credentials") {

//         const res = await sendRequest<JWT>({
//           url: "http://localhost:8000/auth/social-media",
//           method: "POST",
//           body: {
//             type: account?.provider.toLocaleUpperCase(),
//             username: user.email
//           }
//         })

//         console.log("check data", res)
//         console.log("check user", user)
//         console.log("check token", token)


//         if (res) {
//             token.access_token = res.access_token,
//             token.refresh_token = res.refresh_token,
//             token.user = res.user
//         }
//       }

//       if (trigger === "signIn" && account?.provider === "credentials") {
//           // @ts-ignore
//           token.access_token = user.access_token,
//           // @ts-ignore
//           token.refresh_token = user.refresh_token,
//           // @ts-ignore
//           token.user = user.user
//       }
//       return token;
//     },



//     session({ session, token, user }) {
//       // console.log("check session", session)
//       // console.log("check token", token)
//       if (token) {
//         session.access_token = token.access_token,
//           session.refresh_token = token.refresh_token,
//           session.user = token.user
//       }
//       return session
//     },
//   },
// }


const handler = NextAuth(authOptions)

export { handler as GET, handler as POST }