import { authOptions } from "@/app/api/auth/[...nextauth]/route"
import PlaylistSub from "@/component/manager/admin/playlist/playlist.table"
import { sendRequest } from "@/utils/api"
import { playlistListPagination } from "@/utils/api/admin/playlist.api"
import { getServerSession } from "next-auth"



const PlaylistPage = async () => {

    const session = await getServerSession(authOptions)

    const res = await playlistListPagination(session?.access_token || '')

    return (
        <div>
            <PlaylistSub
                data={res.result ?? []}
                meta={res.meta ?? []}
            />
        </div>
    )
}

export default PlaylistPage