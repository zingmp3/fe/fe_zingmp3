import { authOptions } from "@/app/api/auth/[...nextauth]/route";
import TrackSub from "@/component/manager/admin/track/track.table";
import { trackListPagination } from "@/utils/api/admin/track.api";
import { getServerSession } from "next-auth";


const TrackPage = async () => {

    const session = await getServerSession(authOptions)

    const res = await trackListPagination(session?.access_token || '')



    return (
        <>

            <TrackSub
                data={res.result ?? []}
                meta={res.meta ?? []}
            />

        </>

    )
}

export default TrackPage;