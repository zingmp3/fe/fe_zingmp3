import { authOptions } from "@/app/api/auth/[...nextauth]/route"
import UserSub from "@/component/manager/admin/user/user.table"
import { userListPagination } from "@/utils/api/admin/use.api"
import { getServerSession } from "next-auth"




const UserPage = async () => {
    const session = await getServerSession(authOptions)

    const res = await userListPagination(session?.access_token || '')

    return (
        <>
            <UserSub
                data={res.result ?? []}
                meta={res.meta ?? []}
            />
        </>
    )
}

export default UserPage
