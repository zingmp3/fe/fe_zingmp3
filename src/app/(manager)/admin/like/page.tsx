import { authOptions } from "@/app/api/auth/[...nextauth]/route"
import LikeSub from "@/component/manager/admin/like/like.table"
import { likeListPagination } from "@/utils/api/admin/like.api"
import { getServerSession } from "next-auth"



// console.log(">>>Refresh token trong server component:", session?.access_token.slice(-5) ?? "")


const LikePage = async () => {
    const session = await getServerSession(authOptions)

    const res = await likeListPagination(session?.access_token || "", session?.user._id)

    return (
        <div>
            <LikeSub
                data={res.result ?? []}
                meta={res.meta ?? []}

            />
        </div>
    )
}

export default LikePage