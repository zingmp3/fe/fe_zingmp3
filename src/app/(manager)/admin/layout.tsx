import AppHeaderAdmin from "@/component/manager/admin/header/header.admin"
import NextAuthWrapper from "@/lib/next.auth.wrapper"


export default function RootLayout({
  children,
}: {
  children: React.ReactNode
}) {
  return (
    <>
      <NextAuthWrapper>
        <AppHeaderAdmin>
          {children}
        </AppHeaderAdmin>
      </NextAuthWrapper>



    </>

  )
}