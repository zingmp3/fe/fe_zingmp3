import { authOptions } from "@/app/api/auth/[...nextauth]/route"
import CommentSub from "@/component/manager/admin/comment/comment.table"
import { commentListPagination } from "@/utils/api/admin/comment.api"
import { getServerSession } from "next-auth"





const CommentPage = async () => {
    const session = await getServerSession(authOptions)

    const res = await commentListPagination(session?.access_token || "", session?.user._id)

    return (
        <>
            <CommentSub
                data={res.result ?? []}
                meta={res.meta ?? []}

            />
        </>

    )
}

export default CommentPage