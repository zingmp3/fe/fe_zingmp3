import { authOptions } from "@/app/api/auth/[...nextauth]/route"
import AuthSub from "@/component/guest/auth/auth.user"
import { getServerSession } from "next-auth"

import { redirect } from "next/navigation"



const SignInPage = async () => {

  const session = await getServerSession(authOptions)

  if (session) {
    // redirect to homepage
    redirect("/")
  }

  return (
    <>
      <div>
        <AuthSub />
      </div>
    </>
  )
}

export default SignInPage