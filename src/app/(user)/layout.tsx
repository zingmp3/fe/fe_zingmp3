import FooterUser from "@/component/user/footer/footer.user"
import AppHeaderUser from "@/component/user/header/header.user"
import NextAuthWrapper from "@/lib/next.auth.wrapper"
import { TrackContextProvider } from "@/lib/track.wrapper"
import { getServerSession } from "next-auth"
import { authOptions } from "../api/auth/[...nextauth]/route"
import { getAllTrack } from "@/utils/api/user/track.api"



export default async function RootLayout({
  children,
}: {
  children: React.ReactNode
}) {



  const session = await getServerSession(authOptions)

  // console.log("check session:", session)

  const tracks = await getAllTrack(session?.access_token)


  return (


    <NextAuthWrapper>
      <TrackContextProvider>
        <AppHeaderUser track={tracks.result}>
          {children}
        </AppHeaderUser>
        <FooterUser />
      </TrackContextProvider>
    </NextAuthWrapper>


  )
}