import { authOptions } from "@/app/api/auth/[...nextauth]/route"
import LibrarySub from "@/component/user/library/library.user"
import { getAllLike, getAllPlaylist } from "@/utils/api/user/library.api"
import { getServerSession } from "next-auth"



const LibraryPage = async () => {


    const session = await getServerSession(authOptions)

    const playlist = await getAllPlaylist(
        session?.access_token,
        session?.user._id
    )

    const like = await getAllLike(
        session?.user._id,
        session?.access_token,
    )


    return (
        <>
            <LibrarySub
                playlist={playlist.result ?? []}
                like={like.result ?? []}
            />

        </>
    )
}

export default LibraryPage