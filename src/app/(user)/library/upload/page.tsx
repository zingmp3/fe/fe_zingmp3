

import { authOptions } from "@/app/api/auth/[...nextauth]/route"
import UploadUserSub from "@/component/user/library/upload/upload.user"
import { getAllPlaylist } from "@/utils/api/user/library.api"
import { getAllUpload } from "@/utils/api/user/upload.api"
import { getServerSession } from "next-auth"


const UploadPage = async () => {


    const session = await getServerSession(authOptions)

    const playlist = await getAllPlaylist(
        session?.access_token,
        session?.user._id
    )



    const uploads = await getAllUpload(
        session?.access_token
    )

    console.log("check upload:", uploads)


    return (
        <>
            <UploadUserSub
                playlist={playlist.result ?? []}
            uploads={uploads.reverse()}
            />

        </>
    )
}

export default UploadPage
