
import { authOptions } from "@/app/api/auth/[...nextauth]/route"
import LikeUserSub from "@/component/user/library/like/like.user"
import { getAllLike, getAllPlaylist } from "@/utils/api/user/library.api"

import { getServerSession } from "next-auth"


const LikeUserPage = async () => {


    const session = await getServerSession(authOptions)

    const playlist = await getAllPlaylist(
        session?.access_token,
        session?.user._id
    )

    const like = await getAllLike(
        session?.user._id,
        session?.access_token,
    )



console.log("check like:", like)

    return (
        <>
            <LikeUserSub
                playlist={playlist.result ?? []}
                like={like.result ?? []}
            />

        </>
    )
}

export default LikeUserPage