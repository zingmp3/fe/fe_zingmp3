import { authOptions } from "@/app/api/auth/[...nextauth]/route"
import DetailPlaylistSub from "@/component/user/library/detail_playlist/detailPlaylist.user"
import { getDetailPlaylist } from "@/utils/api/user/detailPlaylist.api"
import { getAllLike, getAllPlaylist } from "@/utils/api/user/library.api"
import { getServerSession } from "next-auth"


const DetailPlaylist = async (props: any) => {
    const { params } = props
    const session = await getServerSession(authOptions)

    // console.log("check props", params.slug)

    const playlist = await getDetailPlaylist(
        params.slug,
        session?.access_token
    )

    // console.log("check playlist", playlist)



    const playlistDetail = await getAllPlaylist(
        session?.access_token,
        session?.user._id
    )

    const like = await getAllLike(
        session?.user._id,
        session?.access_token,
    )




    return (
        <>
            <DetailPlaylistSub
                playlist={playlist ?? []}
                playlistDetail={playlistDetail.result}
                like={like.result}
            />
        </>
    )
}

export default DetailPlaylist