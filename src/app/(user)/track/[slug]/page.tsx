
import { authOptions } from '@/app/api/auth/[...nextauth]/route'
import TracksSub from '@/component/user/track/trackdetail.user'
import { getAllLike, getAllPlaylist } from '@/utils/api/user/library.api'
import { getAllComment, getTracksDetail } from '@/utils/api/user/track.api'
import { getServerSession } from 'next-auth'





const TracksPage = async (props: any) => {
    const { params } = props

    console.log("check props", params.slug)

    const session = await getServerSession(authOptions)


    const track = await getTracksDetail(
        params.slug,
        session?.access_token
    )

    const comment = await getAllComment(
        params.slug,
        session?.access_token
    )

    

    const like = await getAllLike(
        session?.user._id,
        session?.access_token,
    )

    
    const playlistDetail = await getAllPlaylist(
        session?.access_token,
        session?.user._id
    )

    return (
        <>
            <TracksSub
                track={track ?? []}
                comment={comment.result ?? []}
                like={like.result ?? []}
                paramsId={params.slug ?? ''}
                playlistDetail={playlistDetail.result}
            />
        </>
    )
}

export default TracksPage