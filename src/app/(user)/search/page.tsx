import { authOptions } from "@/app/api/auth/[...nextauth]/route"
import SearchSub from "@/component/user/search/search.user"
import { querySearchTracks } from "@/utils/api/user/search.api"
import { getServerSession } from "next-auth"
import { Suspense } from "react"

const SearchPage = async({ searchParams }: { searchParams: { q: string } }) => {

    const params = searchParams.q;


    console.log("check params:", params)
    const session = await getServerSession(authOptions)


        const tracksSearch = await querySearchTracks(
            params,
            session?.access_token,
            
        )

    return (

        <>
            <Suspense fallback={<div>Loading...</div>}>
                <SearchSub 
                tracks={tracksSearch}
                params={params}
                />
            </Suspense>
        </>

    )
}

export default SearchPage