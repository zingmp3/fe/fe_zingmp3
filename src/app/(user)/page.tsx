import { getServerSession } from "next-auth"
import DiscoverSub from "@/component/user/discover/discover.user"
import { authOptions } from "@/app/api/auth/[...nextauth]/route"
import { getAllTrack } from "@/utils/api/user/track.api"




const DiscoverPage = async () => {


    const session = await getServerSession(authOptions)

    console.log("check session:", session)

    const tracks = await getAllTrack(session?.access_token)


    return (
        <>
            <DiscoverSub
                data={tracks.result ?? []}

            />
        </>
    )
}

export default DiscoverPage