
'use client';

import { createContext, useContext, useState } from "react";

const initValue = {
    _id: "string",
    title: "string",
    singer: "string",
    category: "string",
    imgUrl: "string",
    trackUrl: "string",
    isPlaying: false,
}
//  hàm TrackContextProvider sẽ được lấy giá trị "value"  thông qua createContext() 
export const TrackContext = createContext({})

export const TrackContextProvider = ({ children }: { children: React.ReactNode }) => {
    const [currentTrack, setCurrentTrack] = useState<IShareTrack>(initValue);

    return (
        <TrackContext.Provider value={{ currentTrack, setCurrentTrack }}>
            {children}
        </TrackContext.Provider>
    )
};

export const useTrackContext = () => useContext(TrackContext);  //  khởi tạo hàm để return useContext(TrackContext) nên dùng useTrackContext thay