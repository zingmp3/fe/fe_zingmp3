'use client'
import { SessionProvider } from "next-auth/react"

export default function NextAuthWrapper({
  children,
}: {
  children: React.ReactNode
}) {
  return (
    <html lang="en" suppressHydrationWarning>
      <SessionProvider>
        <body>{children}</body>
      </SessionProvider>
    </html>


  )
}