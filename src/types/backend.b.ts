
export { };

declare global {
    interface IRequest {
        url: string;
        method: string;
        body?: { [key: string]: any };
        queryParams?: any;
        useCredentials?: boolean;
        headers?: any;
        nextOption?: any;
        credentials?: 'include';
    }

    interface IBackendRes<T> {
        error?: string | string[];
        message: string;
        statusCode: number | string;
        result?: T;
    }

    interface IModelPaginate<T> {
        meta: {
            current: number;
            pageSize: number;
            pages: number;
            total: number;
        },
        result: T[]
    }

    interface ITrackComment {

        _id: string,
        content: string,
        userId: {
            _id: string,
            email: string,
            name: string,
            age: number,
            address: string,
            role: string,
        }
        trackId: {
            _id: string,
            title: string,
            category: string
        }
        isDeleted: boolean,
        createdAt: string,
        updatedAt: string,
        deletedAt: string,
    }


    interface IUser {
        _id: string,
        email: string,
        password: string
        name: string,
        username: string,
        age: number,
        address: string,
        type: string,
        role: string,
    }

    interface ILike {
        _id: string,
        userId: {
            _id: string,
            email: string
        },
        tracks: [
            {
                _id?: string,
                title?: string,
                singer?: string,
                category?: string,
                imgUrl?: string,
                trackUrl?: string,
            }
        ],

    }

    interface IPlaylist {
        _id: string,
        title: string,
        userId: {
            _id: string,
            email: string
        },
        tracks: [
            {
                _id: string,
                title: string,
                singer: string,
                category: string,
                imgUrl: string,
                trackUrl: string,
            }
        ]
    }

    interface ITracks {
        _id: string,
        title: string
        singer: string,
        category: string,
        imgUrl: string,
        trackUrl: string,
    }

    interface IShareTrack extends ITracks {
        isPlaying?: boolean
    }

    interface ITrackContext {
        currentTrack: IShareTrack;
        setCurrentTrack: (v: IShareTrack) => void;
    }
}