


Route Cache: Lưu trữ thông tin điều hướng (routes) của ứng dụng để xử lý nhanh hơn các yêu cầu đến.
Data Cache: Lưu trữ dữ liệu đã được truy xuất hoặc tính toán để sử dụng lại trong tương lai nhằm cải thiện hiệu suất và giảm tải hệ thống.